<?php
	$tplSignup= new Xtemplate('view/account/signup.htm');
	global $dbs;
	global $appdb;
	global $baseUrl;
	global $mailvalid;
	$errMes='';
	$do_save=0;
	$url='./img/avartar/';
	$allowType=array('jpg','png','jpeg');
	$maxSize=1024000;
	if($_POST){
		$flogo=!empty($_FILES['flogo'])?$_FILES['flogo']:'';
		$email=$_POST['txtEmail'];
		$pwd=$_POST['txtMatkhau'];
		$apwd=$_POST['txtNhapLaiMK'];
		$hoten=$_POST['txtHoTen'];
		$ngaysinh=$_POST['cbxngaysinh'];
		$thangsinh=$_POST['cbxthangsinh'];
		$namsinh=$_POST['cbxnamsinh'];
		$gioitinh=$_POST['cbxGioiTinh'];
		$tthonnhan=$_POST['cbxTTHonNhan'];
		$diachi=$_POST['txtDiaChi'];
		$dienthoai=$_POST['txtDienThoai'];
		
		
		$birth=$ngaysinh."-".$thangsinh."-".$namsinh;
		$dbirth=date('Y-m-d',strtotime($birth));
		$now=date('Y-m-d');
		
		$sql="SELECT Email FROM tblNguoiTimViec WHERE Email='$email'";
		$arrEmail=$dbs->fetchAll($sql);
	
		$sender = 'nguyenthanhtrung.dthn@gmail.com';
		$results = $mailvalid->validate(array($email), $sender);
			
		
		if(!$email){
			$errMes.='<li>Email không được để trống</li>';
			$do_save=-1;
			}
			elseif(!isEmail($email)){
				$errMes.='<li>Email không hợp lệ</li>';
				$do_save=-1;
			}
				elseif(!$results[$email]){
					$errMes.='<li>Email không tồn tại</li>';
					$do_save=-1;
				}
				elseif($arrEmail!='-1'){
					$errMes.='<li>Email đã được đăng ký</li>';
					$do_save=-1;
				}
				
		if(!$pwd){
			$errMes.='<li>Mật khẩu không được để trống</li>';
			$do_save=-1;	
		}
		if(!$apwd){
			$errMes.='<li>Nhập lại mật khẩu không được để trống</li>';
			$do_save=-1;
			}
			elseif($pwd!=$apwd){
				$errMes.='<li>Trường Mật khẩu và Nhập lại mật khẩu phải giống nhau</li>';
				$do_save=-1;
			}	
		if(!$hoten){
			$errMes.='<li>Họ tên không được để trống</li>';
			$do_save=-1;	
		}
		if(!$ngaysinh||!$thangsinh||!$namsinh){
			$errMes.='<li>Hãy chọn ngày sinh</li>';
			$do_save=-1;	
		}elseif(strtotime($birth)>strtotime($now)){
			$errMes.='<li>Ngày sinh phải nhỏ hơn ngày hiện tại</li>';
			$do_save=-1;
			}
		if(!$gioitinh){
			$errMes.='<li>Hãy chọn giới tính</li>';
			$do_save=-1;	
		}
		if(!$tthonnhan){
			$errMes.='<li>Hãy chọn tình trạng hôn nhân</li>';
			$do_save=-1;	
		}
		if(!$diachi){
			$errMes.='<li>Địa chỉ không được để trống</li>';
			$do_save=-1;	
		}
		
		if(!$dienthoai){
			$errMes.='<li>Điện thoại không được để trống</li>';
			$do_save=-1;
			}
			else if(!preg_match("/^[0-9]+$/", $dienthoai)){
				$errMes.='<li>Điện thoại không hợp lệ</li>';
				$do_save=-1;		
				
		}
		
		if($errMes!=''){
			$tplSignup->assign('errMes',$errMes);
		}
		if($do_save!=-1){
			if($flogo['name']!=''){
				$file_AnhDaiDien=uploadFile($flogo, $url, $allowType, $maxSize);
				if($file_AnhDaiDien=='110i'){
					alert('Kich cỡ file quá lớn');
				}
				if($file_AnhDaiDien=='1200'){
					alert('Định dạng file không hợp lệ');
				}
			}
			$arrData=array('Email'=>$email
							,'Pwd'=>$pwd
							,'TenNTV'=>$hoten
							,'NgaySinh'=>$dbirth
							,'GioiTinh'=>$gioitinh
							,'LinkAnhDaiDien'=>$file_AnhDaiDien
							,'TinhTrangHonNhan'=>$tthonnhan
							,'DiaChi'=>$diachi
							,'DTLienHe'=>$dienthoai);
							
			
			//Save data to tbladvetising
			if($dbs->insert('tblnguoitimviec',$arrData)){
				alert('Chúc mừng! Bạn đã đăng ký thành công! Hãy đăng nhập để truy cập hệ thống.');
				redir('?m=account&a=login');
			}
		}
		//Gan gia tri cac bien sau khi nhan nut post
		$tplSignup->assign('Email',$email);
		$tplSignup->assign('Pwd',$pwd);
		$tplSignup->assign('LinkAnhDaiDien',$flogo['name']);
		$tplSignup->assign('TenNTV',$hoten);
		$tplSignup->assign('DiaChi',$diachi);
		$tplSignup->assign('DTLienHe',$dienthoai);
	}

	
	$tplSignup->parse('SIGNUP');
	$left_content= $tplSignup->text('SIGNUP');
?>
