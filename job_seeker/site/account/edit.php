<?php
	$tplSignup= new Xtemplate('view/account/signup.htm');
	global $dbs;
	global $appdb;
	global $baseUrl;
	global $mailvalid;
	$errMes='';
	$do_save=0;
	
	if($_POST){
		$email=$_POST['txtEmail'];
		$pwd=$_POST['txtMatkhau'];
		$apwd=$_POST['txtNhapLaiMK'];
		$hoten=$_POST['txtHoTen'];
		$ngaysinh=$_POST['cbxngaysinh'];
		$thangsinh=$_POST['cbxthangsinh'];
		$namsinh=$_POST['cbxnamsinh'];
		$gioitinh=$_POST['cbxGioiTinh'];
		$tthonnhan=$_POST['cbxTTHonNhan'];
		$diachi=$_POST['txtDiaChi'];
		$dienthoai=$_POST['txtDienThoai'];
		$check=$_POST['chkDongY'];
		
		$birth=$ngaysinh."-".$thangsinh."-".$namsinh;
		$dbirth=date('Y-m-d',strtotime($birth));
		$now=date('Y-m-d');
		
		$sql="SELECT Email FROM tblNguoiTimViec WHERE Email='$email'";
		$arrEmail=$dbs->fetchAll($sql);
	
			$sender = 'nguyenthanhtrung.dthn@gmail.com';
			$results = $mailvalid->validate(array($email), $sender);
			
		
		if(!$email){
			$errMes.='<li>Email không được để trống</li>';
			$do_save=-1;
			}
			elseif(!isEmail($email)){
				$errMes.='<li>Email không hợp lệ</li>';
				$do_save=-1;
			}
				elseif(!$results[$email]){
					$errMes.='<li>Email không tồn tại</li>';
					$do_save=-1;
				}
				elseif($arrEmail!='-1'){
					$errMes.='<li>Email đã được đăng ký</li>';
					$do_save=-1;
				}
				
		if(!$pwd){
			$errMes.='<li>Mật khẩu không được để trống</li>';
			$do_save=-1;	
		}
		if(!$apwd){
			$errMes.='<li>Nhập lại mật khẩu không được để trống</li>';
			$do_save=-1;
			}
			elseif($pwd!=$apwd){
				$errMes.='<li>Trường Mật khẩu và Nhập lại mật khẩu phải giống nhau</li>';
				$do_save=-1;
			}	
		if(!$hoten){
			$errMes.='<li>Họ tên không được để trống</li>';
			$do_save=-1;	
		}
		if(!$ngaysinh||!$thangsinh||!$namsinh){
			$errMes.='<li>Hãy chọn ngày sinh</li>';
			$do_save=-1;	
		}elseif(strtotime($birth)>strtotime($now)){
			$errMes.='<li>Ngày sinh phải nhỏ hơn ngày hiện tại</li>';
			$do_save=-1;
			}
		if(!$gioitinh){
			$errMes.='<li>Hãy chọn giới tính</li>';
			$do_save=-1;	
		}
		if(!$tthonnhan){
			$errMes.='<li>Hãy chọn tình trạng hôn nhân</li>';
			$do_save=-1;	
		}
		if(!$diachi){
			$errMes.='<li>Địa chỉ không được để trống</li>';
			$do_save=-1;	
		}
		
		if(!$dienthoai){
			$errMes.='<li>Điện thoại không được để trống</li>';
			$do_save=-1;
			}
			else if(!is_numeric($dienthoai)){
				$errMes.='<li>Điện thoại không hợp lệ</li>';
				$do_save=-1;		
				
		}
		if(!$check){
			$errMes.='<li>Bạn có đồng ý các điều khoản sử dụng	</li>';
			$do_save=-1;	
		}
		if($errMes!=''){
			$tplSignup->assign('errMes',$errMes);
		}
		if($do_save!=-1){
			//Xu ly img, neu chon anh thi upload file
			//if($file['name']!=''){
//				$image=uploadFile($file, $url, $allowType, $maxSize);
//				if($image=='110i'){
//					alert('Kích thước tệp quá lớn!');
//				}
//				if($image=='1200'){
//					alert('Loai tệp không phù hợp!');
//				}
			//Create array to save database
			$arrData=array('Email'=>$email
							,'Pwd'=>$pwd
							,'TenNTV'=>$hoten
							,'NgaySinh'=>$dbirth
							,'GioiTinh'=>$gioitinh
							,'TinhTrangHonNhan'=>$tthonnhan
							,'MaTT'=>$tinhthanh
							,'DiaChi'=>$diachi
							,'DTLienHe'=>$dienthoai
							,'MaTT'=>$tinhthanh);
			
			//Save data to tbladvetising
			if($dbs->insert('tblnguoitimviec',$arrData)){
				alert('Đăng ký thành công!');
				redir('?m=account&a=login');
			}
		}
		//Gan gia tri cac bien sau khi nhan nut post
		$tplSignup->assign('Email',$email);
		$tplSignup->assign('Pwd',$pwd);
		$tplSignup->assign('TenNTV',$hoten);
		$tplSignup->assign('MaTT',$tinhthanh);
		$tplSignup->assign('DiaChi',$diachi);
		$tplSignup->assign('DTLienHe',$dienthoai);
	}

	
	$tplSignup->assign('tinhthanh',$listtinhthanh);
	$tplSignup->parse('SIGNUP');
	$left_content= $tplSignup->text('SIGNUP');
?>
