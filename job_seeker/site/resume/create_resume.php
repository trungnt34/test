<?php
	global $dbs;
	global $baseUrl;
	global $appdb;
	$email=@$_SESSION['email'];
	$sql="select * from tblhoso as H 
			inner join tblNguoitimviec as V on V.MaNTV=H.MaNTV
			where Email='$email'";
	$rsHoSo=$dbs->fetchAll($sql);
	$total=count($rsHoSo);	
	if(@$_SESSION['email']==''){
		alert('Bạn phải đăng nhập trước khi sử dụng chức năng này!');
		redir('?m=account&a=login');
	}else if($total>=5){
		alert('Bạn chỉ được tạo tối đa 5 hồ sơ!');
		redir('?m=resume&a=lst_resume');
	}else{
		
		$errMes='';
		$tplCreate_resume= new Xtemplate('view/resume/create_resume.htm');
		global $dbs;
		global $baseUrl;
		
		
		$sql1="select * from tblnguoitimviec 
				where Email='$email'";
		$rs=$dbs->fetchOne($sql1);
		
		$TenNTV=$rs['TenNTV'];
		$NgaySinh=date('d-m-Y',strtotime($rs['NgaySinh']));
		$GioiTinh=$rs['GioiTinh'];
		$TinhTrangHonNhan=$rs['TinhTrangHonNhan'];
		$DiaChi=$rs['DiaChi'];
		$DTLienHe=$rs['DTLienHe'];
		$MaNTV=$rs['MaNTV'];
		
		$tplCreate_resume->assign('TenNTV',$TenNTV);
		$tplCreate_resume->assign('NgaySinh',$NgaySinh);
		$tplCreate_resume->assign('GioiTinh',$GioiTinh);
		$tplCreate_resume->assign('TinhTrangHonNhan',$TinhTrangHonNhan);
		$tplCreate_resume->assign('DiaChi',$DiaChi);
		$tplCreate_resume->assign('DTLienHe',$DTLienHe);
		$tplCreate_resume->assign('Email',$email);
		
		$lstNganhNghe=$appdb->getComboBox('tblnganhnghe','MaNganh','TenNganh','cbxNganhNghe','...');
		$lstTinhThanh=$appdb->getComboBox('tbltinhthanh','MaTT','TenTT','cbxTinhThanh','...');
				if($_POST){
		
			//Tieu de
			$TieuDe=$_POST['txtTieuDe'];
			if(!$TieuDe){
				$errMes.="<li>- Tiêu đề không được để trống</li>";
				$do_save=-1;
			}
			
			//Trinh do hoc van	
			$LoaiBC=$_POST['cbxhocvan'];
			$NoiDaoTao=$_POST['txtNoiDaoTao'];
			$ChuyenNganh=$_POST['txtChuyenNganh'];
			$thangbd=substr($_POST['cbxthangBD'],-1);
			$txtthangbd="Tháng ".$thangbd;
			$nambd=substr($_POST['cbxnamBD'],-4);
			$txtnambd=$nambd;

			$thangkt=substr($_POST['cbxthangKT'],-1);
			$txtthangkt="Tháng ".$thangkt;

			$namkt=substr($_POST['cbxnamKT'],-4);
			$txtnamkt=$namkt;

			$XepLoai=$_POST['txtXepLoai'];
			$MoTa=$_POST['txtaMoTa'];
			//xu ly ngay thang
			$txtNgayBatDau="1-".$thangbd."-".$nambd;
			$NgayBatDau=date('Y-m-d',strtotime($txtNgayBatDau));
			$txtNgayKetThuc="1-".$thangkt."-".$namkt;
			$NgayKetThuc=date('Y-m-d',strtotime($txtNgayKetThuc));
			if(!$LoaiBC){
				$errMes.="<li>- Hãy chọn trình độ học vấn</li>";
				$do_save=-1;
			}
			if(!$NoiDaoTao){
				$errMes.="<li>- Nơi đào tạo không được để trống</li>";
				$do_save=-1;
			}
			if(!$ChuyenNganh){
				$errMes.="<li>- Chuyên ngành không được để trống</li>";
				$do_save=-1;
			}
			if(!$thangbd||!$nambd){
				$errMes.="<li>- Hãy chọn thời gian bắt đầu khóa học</li>";
				$do_save=-1;
			}else if(strtotime($NgayBatDau)>=strtotime($NgayKetThuc)){
				$errMes.="<li>- Thời gian đào tạo không hợp lệ</li>";
				$do_save=-1;
			}	
			if(!$thangkt||!$namkt){
				$errMes.="<li>- Hãy chọn thời gian kết thúc khóa học</li>";
				$do_save=-1;
			}
			if(!$XepLoai){
				$errMes.="<li>- Xếp loại không được để trống</li>";
				$do_save=-1;
			}
			
			
			//Kinh nghiem
			$ThoiGianLamViec=$_POST['cbxThoiGianLamViec'];
			if(!$ThoiGianLamViec){
				$errMes.="<li>- Kinh nghiệm không được để trống</li>";
				$do_save=-1;
			}
			$CongTy=$_POST['txtCongty'];
			$ViTri=$_POST['txtVitri'];
			$NhiemVuChinh=$_POST['txtNhiemvu'];
			$ThanhTich=$_POST['txtaThanhTich'];
			
			
			
			//Ki nang
			
			$KiNangChuyenMon=$_POST['txtaKNChuyenMon'];
			$KiNangKhac=$_POST['txtaKNkhac'];
			if(!$KiNangChuyenMon){
				$errMes.="<li>- Kĩ năng chuyên môn không được để trống</li>";
				$do_save=-1;
			}	
			
			
			//Cong viec mong muon
			$MaTT=$_POST['cbxTinhThanh'];
			$MaNganh=$_POST['cbxNganhNghe'];
			$LoaiCV=$_POST['cbxLoaiCVMM'];
			$ViTriMM=$_POST['txtVitriMM'];
			$MucLuong=$_POST['txtMucLuong'];
			$DVTinhLuong=$_POST['cbxDVTinhLuong'];
			$MucTieuNgheNghiep=$_POST['txtMuctieu'];
			$DiCongTac=$_POST['cbxCongtac'];
			
			if(!$MaNganh){
				$errMes.="<li>- Hãy chọn ngành nghề mong muốn</li>";
				$do_save=-1;
			}
			if(!$MaTT){
				$errMes.="<li>- Hãy chọn nơi làm việc mong muốn</li>";
				$do_save=-1;
			}
			if(!$LoaiCV){
				$errMes.="<li>- Hãy chọn loại công việc mong muốn</li>";
				$do_save=-1;
			}
			if(!$ViTriMM){
				$errMes.="<li>- Hãy chọn vị trí mong muốn</li>";
				$do_save=-1;
			}
			if(!$MucLuong){
				$errMes.="<li>- Hãy chọn mức lương mong muốn</li>";
				$do_save=-1;
			}
			if(!$DVTinhLuong){
				$errMes.="<li>- Hãy chọn đơn vị tiền tệ</li>";
				$do_save=-1;
			}
			if(!$DiCongTac){
				$errMes.="<li>- Bạn có thể đi công tác không?</li>";
				$do_save=-1;
			}
			
			
			if($errMes!=''){
				$tplCreate_resume->assign('errMes',$errMes);
			}
		$tplCreate_resume->assign('TieuDe',$TieuDe);
		$tplCreate_resume->assign('NoiDaoTao',$NoiDaoTao);
		$tplCreate_resume->assign('ChuyenNganh',$ChuyenNganh);
		$tplCreate_resume->assign('txtthangbd',$txtthangbd);
		$tplCreate_resume->assign('txtnambd',$txtnambd);
		$tplCreate_resume->assign('txtthangkt',$txtthangkt);
		$tplCreate_resume->assign('txtnamkt',$txtnamkt);
		$tplCreate_resume->assign('XepLoai',$XepLoai);
		$tplCreate_resume->assign('MoTa',$MoTa);
		
		$tplCreate_resume->assign('ThoiGianLamViec',$ThoiGianLamViec);
		$tplCreate_resume->assign('CongTy',$CongTy);
		$tplCreate_resume->assign('ViTri',$ViTri);
		$tplCreate_resume->assign('NhiemVuChinh',$NhiemVuChinh);
		$tplCreate_resume->assign('ThanhTich',$ThanhTich);
		
		$tplCreate_resume->assign('KiNangChuyenMon',$KiNangChuyenMon);
		$tplCreate_resume->assign('KiNangKhac',$KiNangKhac);
		$tplCreate_resume->assign('LoaiCV',$LoaiCV);
		$tplCreate_resume->assign('ViTriMM',$ViTriMM);
		$tplCreate_resume->assign('MucLuong',$MucLuong);
		$tplCreate_resume->assign('DVTinhLuong',$DVTinhLuong);
		$tplCreate_resume->assign('MucTieuNgheNghiep',$MucTieuNgheNghiep);
		$tplCreate_resume->assign('DiCongTac',$DiCongTac);
		$tplCreate_resume->assign('LoaiBC',$LoaiBC);
		$tplCreate_resume->assign('DiCongTac',$DiCongTac);
		$tplCreate_resume->assign('MaNganh',$MaNganh);
		$tplCreate_resume->assign('MaTT',$MaTT);

		if($do_save!=-1){
			$arrData1=array('TieuDe'=>$TieuDe
							,'KiNangChuyenMon'=>$KiNangChuyenMon
							,'KiNangKhac'=>$KiNangKhac
							,'MaNTV'=>$MaNTV
							,'MucLuong'=>$MucLuong
							,'DVTinhLuong'=>$DVTinhLuong
							,'MucTieuNgheNghiep'=>$MucTieuNgheNghiep
							,'ViTriMM'=>$ViTriMM
							,'LoaiCV'=>$LoaiCV
							,'DiCongTac'=>$DiCongTac
							,'NgaySuaDoi'=>date('Y-m-d')
							,'TrangThai'=>'Bản tạm'
							,'MaNganh'=>$MaNganh
							,'MaTT'=>$MaTT);
							
			if($dbs->insert('tblHoSo',$arrData1)){
				$sqlMaHS="SELECT Max(MaHS) from tblhoso where MaNTV='$MaNTV'";
				$arrMaHS=$dbs->fetchOne($sqlMaHS);
				$MaHS=$arrMaHS['Max(MaHS)'];
					
				$arrData2=array('LoaiBC'=>$LoaiBC
							,'NoiDaoTao'=>$NoiDaoTao
							,'ChuyenNganh'=>$ChuyenNganh
							,'NgayBatDau'=>$NgayBatDau
							,'NgayKetThuc'=>$NgayKetThuc
							,'XepLoai'=>$XepLoai
							,'MoTa'=>$MoTa
							,'MaHS'=>$MaHS);
				$arrData3=array('CongTy'=>$CongTy
							,'ViTri'=>$ViTri
							,'NhiemVuChinh'=>$NhiemVuChinh
							,'ThoiGianLamViec'=>$ThoiGianLamViec
							,'ThanhTich'=>$ThanhTich
							,'MaHS'=>$MaHS);
				if($dbs->insert('tblbangcap',$arrData2)&&$dbs->insert('tblkinhnghiem',$arrData3)){
				alert('Tạo hồ sơ thành công!');
				redir('?m=resume&a=lst_resume');
				}
			}else{
				alert('Lỗi hệ thống! Bạn vui lòng kiểm tra lại các thông tin đã nhập!');
			}
		}
	}		
		$tplCreate_resume->assign('lstNganhNghe',$lstNganhNghe);
		$tplCreate_resume->assign('lstTinhThanh',$lstTinhThanh);

		$tplCreate_resume->parse('CREATE_RESUME');
		$left_content= $tplCreate_resume->text('CREATE_RESUME');
	}
?>
