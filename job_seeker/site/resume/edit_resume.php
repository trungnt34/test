<?php
	global $dbs;
	global $baseUrl;
	global $appdb;		
	if(@$_SESSION['email']==''){
		alert('Bạn phải đăng nhập trước khi sử dụng chức năng này!');
		redir('?m=account&a=login');
	}else{
		$email=@$_SESSION['email'];
		$errMes='';
		$tplEdit_resume= new Xtemplate('view/resume/edit_resume.htm');
		
		$sql="select * from tblnguoitimviec 
				where Email='$email'";
		$rs=$dbs->fetchOne($sql);
		
		$TenNTV=$rs['TenNTV'];
		$NgaySinh=date('d-m-Y',strtotime($rs['NgaySinh']));
		$GioiTinh=$rs['GioiTinh'];
		$TinhTrangHonNhan=$rs['TinhTrangHonNhan'];
		$DiaChi=$rs['DiaChi'];
		$DTLienHe=$rs['DTLienHe'];
		$MaNTV=$rs['MaNTV'];
		
		$id=$_GET['id'];
		$sql1="SELECT * FROM tblHoso WHERE MaHS=$id";
		$rs1=$dbs->fetchOne($sql1);
		$MaHS=$rs1['MaHS'];
		$TieuDe=$rs1['TieuDe'];
		$KiNangChuyenMon=$rs1['KiNangChuyenMon'];
		$KiNangKhac=$rs1['KiNangKhac'];
		$MucLuong=$rs1['MucLuong'];
		$DVTinhLuong=$rs1['DVTinhLuong'];
		$ViTriMM=$rs1['ViTriMM'];
		$LoaiCV=$rs1['LoaiCV'];
		$MucTieuNgheNghiep=$rs1['MucTieuNgheNghiep'];
		$DiCongTac=$rs1['DiCongTac'];
		$MaNganh=$rs1['MaNganh'];
		$MaTT=$rs1['MaTT'];
		
		
		$sql2="SELECT * FROM tblbangcap WHERE MaHS=$id";
		$rs2=$dbs->fetchOne($sql2);
		$LoaiBC=$rs2['LoaiBC'];
		$NoiDaoTao=$rs2['NoiDaoTao'];
		$ChuyenNganh=$rs2['ChuyenNganh'];
		$XepLoai=$rs2['XepLoai'];
		$MoTa=$rs2['MoTa'];
		
		$sql3="SELECT * FROM tblkinhnghiem WHERE MaHS=$id";
		$rs3=$dbs->fetchOne($sql3);
		$ThoiGianLamViec=$rs3['ThoiGianLamViec'];
		$CongTy=$rs3['CongTy'];
		$ViTri=$rs3['ViTri'];
		$NhiemVuChinh=$rs3['NhiemVuChinh'];
		$ThanhTich=$rs3['ThanhTich'];
	
		$lstTinhThanh=$appdb->getComboBox('tblnganhnghe','MaNganh','TenNganh','cbxNganhNghe','...');
		$lstNganhNghe=$appdb->getComboBox('tbltinhthanh','MaTT','TenTT','cbxTinhThanh','...');
		
		if($_POST){
		
			//Tieu de
			$TieuDe=$_POST['txtTieuDe'];
			if(!$TieuDe){
				$errMes.="<li>- Tiêu đề không được để trống</li>";
				$do_save=-1;
			}
			
			//Trinh do hoc van	
			$LoaiBC=$_POST['cbxhocvan'];
			$NoiDaoTao=$_POST['txtNoiDaoTao'];
			$ChuyenNganh=$_POST['txtChuyenNganh'];
			$thangbd=substr($_POST['cbxthangBD'],-1);
			$txtthangbd=$thangbd?"Tháng ".$thangbd:"";
			$nambd=substr($_POST['cbxnamBD'],-4);
			$txtnambd=$nambd;

			$thangkt=substr($_POST['cbxthangKT'],-1);
			$txtthangkt=$thangkt?"Tháng ".$thangkt:"";

			$namkt=substr($_POST['cbxnamKT'],-4);
			$txtnamkt=$namkt;

			$XepLoai=$_POST['txtXepLoai'];
			$MoTa=$_POST['txtaMoTa'];
			//xu ly ngay thang
			$txtNgayBatDau="1-".$thangbd."-".$nambd;
			$NgayBatDau=date('Y-m-d',strtotime($txtNgayBatDau));
			$txtNgayKetThuc="1-".$thangkt."-".$namkt;
			$NgayKetThuc=date('Y-m-d',strtotime($txtNgayKetThuc));
			if(!$LoaiBC){
				$errMes.="<li>- Hãy chọn trình độ học vấn</li>";
				$do_save=-1;
			}
			if(!$NoiDaoTao){
				$errMes.="<li>- Nơi đào tạo không được để trống</li>";
				$do_save=-1;
			}
			if(!$ChuyenNganh){
				$errMes.="<li>- Chuyên ngành không được để trống</li>";
				$do_save=-1;
			}
			if(!$thangbd||!$nambd){
				$errMes.="<li>- Hãy chọn thời gian bắt đầu khóa học</li>";
				$do_save=-1;
			}else if(strtotime($NgayBatDau)>=strtotime($NgayKetThuc)){
				$errMes.="<li>- Thời gian đào tạo không hợp lệ</li>";
				$do_save=-1;
			}	
			if(!$thangkt||!$namkt){
				$errMes.="<li>- Hãy chọn thời gian kết thúc khóa học</li>";
				$do_save=-1;
			}
			if(!$XepLoai){
				$errMes.="<li>- Xếp loại không được để trống</li>";
				$do_save=-1;
			}
			
			
			//Kinh nghiem
			$ThoiGianLamViec=$_POST['cbxThoiGianLamViec'];
			if(!$ThoiGianLamViec){
				$errMes.="<li>- Kinh nghiệm không được để trống</li>";
				$do_save=-1;
			}
			$CongTy=$_POST['txtCongty'];
			$ViTri=$_POST['txtVitri'];
			$NhiemVuChinh=$_POST['txtNhiemvu'];
			$ThanhTich=$_POST['txtaThanhTich'];
			
			//su ly ngay thang
			
			
			//Ki nang
			
			$KiNangChuyenMon=$_POST['txtaKNChuyenMon'];
			$KiNangKhac=$_POST['txtaKNkhac'];
			if(!$KiNangChuyenMon){
				$errMes.="<li>- Kĩ năng chuyên môn không được để trống</li>";
				$do_save=-1;
			}	
			
			
			//Cong viec mong muon
			$MaTT=$_POST['cbxTinhThanh'];
			$MaNganh=$_POST['cbxNganhNghe'];
			$LoaiCV=$_POST['cbxLoaiCVMM'];
			$ViTriMM=$_POST['txtVitriMM'];
			$MucLuong=$_POST['txtMucLuong'];
			$DVTinhLuong=$_POST['cbxDVTinhLuong'];
			$MucTieuNgheNghiep=$_POST['txtMuctieu'];
			$DiCongTac=$_POST['cbxCongtac'];
			
			if(!$MaNganh){
				$errMes.="<li>- Hãy chọn ngành nghề mong muốn</li>";
				$do_save=-1;
			}
			if(!$MaTT){
				$errMes.="<li>- Hãy chọn nơi làm việc mong muốn</li>";
				$do_save=-1;
			}
			if(!$LoaiCV){
				$errMes.="<li>- Hãy chọn loại công việc mong muốn</li>";
				$do_save=-1;
			}
			if(!$ViTriMM){
				$errMes.="<li>- Hãy chọn vị trí mong muốn</li>";
				$do_save=-1;
			}
			if(!$MucLuong){
				$errMes.="<li>- Hãy chọn mức lương mong muốn</li>";
				$do_save=-1;
			}
			if(!$DVTinhLuong){
				$errMes.="<li>- Hãy chọn đơn vị tiền tệ</li>";
				$do_save=-1;
			}
			if(!$DiCongTac){
				$errMes.="<li>- Bạn có thể đi công tác không?</li>";
				$do_save=-1;
			}
			
			
			if($errMes!=''){
				$tplEdit_resume->assign('errMes',$errMes);
			}
		


		if($do_save!=-1){
			$arrData1=array('TieuDe'=>$TieuDe
							,'KiNangChuyenMon'=>$KiNangChuyenMon
							,'KiNangKhac'=>$KiNangKhac
							,'MaNTV'=>$MaNTV
							,'MucLuong'=>$MucLuong
							,'DVTinhLuong'=>$DVTinhLuong
							,'MucTieuNgheNghiep'=>$MucTieuNgheNghiep
							,'ViTriMM'=>$ViTriMM
							,'LoaiCV'=>$LoaiCV
							,'DiCongTac'=>$DiCongTac
							,'NgaySuaDoi'=>date('Y-m-d')
							,'MaNganh'=>$MaNganh
							,'MaTT'=>$MaTT);			
					
			$arrData2=array('LoaiBC'=>$LoaiBC
							,'NoiDaoTao'=>$NoiDaoTao
							,'ChuyenNganh'=>$ChuyenNganh
							,'NgayBatDau'=>$NgayBatDau
							,'NgayKetThuc'=>$NgayKetThuc
							,'XepLoai'=>$XepLoai
							,'MoTa'=>$MoTa);
			$arrData3=array('CongTy'=>$CongTy
							,'ViTri'=>$ViTri
							,'NhiemVuChinh'=>$NhiemVuChinh
							,'ThoiGianLamViec'=>$ThoiGianLamViec
							,'ThanhTich'=>$ThanhTich);
				if($dbs->update('tblHoSo',$arrData1,"MaHS=$MaHS")&&$dbs->update('tblBangCap',$arrData2,"MaHS=$MaHS")&&$dbs->update('tblKinhNghiem',$arrData3,"MaHS=$MaHS")){
				alert('Cập nhật hồ sơ thành công');
				redir('?m=resume&a=lst_resume');
				}
			}
		}
		$tplEdit_resume->assign('TenNTV',$TenNTV);
		$tplEdit_resume->assign('NgaySinh',$NgaySinh);
		$tplEdit_resume->assign('GioiTinh',$GioiTinh);
		$tplEdit_resume->assign('TinhTrangHonNhan',$TinhTrangHonNhan);
		$tplEdit_resume->assign('DiaChi',$DiaChi);
		$tplEdit_resume->assign('DTLienHe',$DTLienHe);
		$tplEdit_resume->assign('Email',$email);
		
		$tplEdit_resume->assign('TieuDe',$TieuDe);
		$tplEdit_resume->assign('NoiDaoTao',$NoiDaoTao);
		$tplEdit_resume->assign('ChuyenNganh',$ChuyenNganh);
		$tplEdit_resume->assign('txtthangbd',$txtthangbd);
		$tplEdit_resume->assign('txtnambd',$txtnambd);
		$tplEdit_resume->assign('txtthangkt',$txtthangkt);
		$tplEdit_resume->assign('txtnamkt',$txtnamkt);
		$tplEdit_resume->assign('XepLoai',$XepLoai);
		$tplEdit_resume->assign('MoTa',$MoTa);
		
		$tplEdit_resume->assign('ThoiGianLamViec',$ThoiGianLamViec);
		$tplEdit_resume->assign('CongTy',$CongTy);
		$tplEdit_resume->assign('ViTri',$ViTri);
		$tplEdit_resume->assign('NhiemVuChinh',$NhiemVuChinh);
		$tplEdit_resume->assign('ThanhTich',$ThanhTich);
		
		$tplEdit_resume->assign('KiNangChuyenMon',$KiNangChuyenMon);
		$tplEdit_resume->assign('KiNangKhac',$KiNangKhac);
		$tplEdit_resume->assign('LoaiCV',$LoaiCV);
		$tplEdit_resume->assign('ViTriMM',$ViTriMM);
		$tplEdit_resume->assign('MucLuong',$MucLuong);
		$tplEdit_resume->assign('DVTinhLuong',$DVTinhLuong);
		$tplEdit_resume->assign('MucTieuNgheNghiep',$MucTieuNgheNghiep);
		$tplEdit_resume->assign('DiCongTac',$DiCongTac);
		$tplEdit_resume->assign('LoaiBC',$LoaiBC);
		$tplEdit_resume->assign('DiCongTac',$DiCongTac);
		$tplEdit_resume->assign('MaNganh',$MaNganh);
		$tplEdit_resume->assign('MaTT',$MaTT);
		
		
		$tplEdit_resume->assign('lstNganhNghe',$lstNganhNghe);
		$tplEdit_resume->assign('lstTinhThanh',$lstTinhThanh);	
		$tplEdit_resume->parse('EDIT_RESUME');
		$left_content= $tplEdit_resume->text('EDIT_RESUME');
	}
?>
