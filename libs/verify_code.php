<?php
	require './config.php';
	$string_alphabet = "0123456789ABCDEFGHYJKLMNOPQRSTUVWXYZ";
	$length_code = 5;
	for($i = 0; $i < $length_code; $i ++) {
		$random_code = rand(0, strlen($string_alphabet) - 1);
		$array_code[$i] = substr($string_alphabet, $random_code, 1);
	}
	$string_code= join("", $array_code);
	$im = imagecreate(55, 16) or die("Cannot Initialize new GD image stream");
	$background_color = imagecolorallocate($im, 255, 255, 255);
	$text_color = imagecolorallocate($im, 55, 55, 55);
	imagestring($im, 6, 1, 0, $string_code, $text_color);
	$_SESSION['verify_code'] = $string_code;
	

	
	imagegif($im);
	imagedestroy($im);
?>