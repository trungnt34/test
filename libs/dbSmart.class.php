<?php
	class dbSmart{
		protected $_host;
		protected $_urs;
		protected $_pwd;
		protected $_dbName;
		
		public function __construct($host,$urs,$pwd,$dbName){
			$this->_host=$host;
			$this->_urs=$urs;
			$this->_pwd=$pwd;
			$this->_dbName=$dbName;
			$cn=@mysql_connect($this->_host,$this->_urs,$this->_pwd) or die('Not connecting to host');
			@mysql_set_charset('utf8',$cn);
			$db=@mysql_select_db($this->_dbName,$cn)or die("Not connected to the <strong>$dbName</strong>");
			
		}
		public function execSQL($sql){
			return mysql_query($sql);
		}
		
		public function fetchOne($sql){
			if($sql){
				$rs=$this->execSQL($sql);
				if(isset($rs)){
					return mysql_fetch_array($rs,MYSQL_ASSOC);
				}else{
					return -1;
				}
			}
		}
		/****
			* return 2dimention array
			*/
		public function fetchAll($sql){
			$arr=array();
			if($sql){
				$rs=$this->execSQL($sql);
				if(isset($rs)){
					
					while($row=mysql_fetch_array($rs,MYSQL_ASSOC)){
						array_push($arr,$row);
					}
				}
			}
			if(!empty($arr)){
				return $arr;
			}else{
				return -1;
			}
		}
		public function delete($table,$condition=null){
			if($condition!=null){
				$sql="DELETE FROM $table WHERE $condition";
			}else{
				$sql="DELETE FROM $table WHERE 1=1";
			}
			if($this->execSQL($sql)){
				return 1;
			}
		}
		/****
			*INSERT INTO  table_name(f1,f2) VALUES(v1,v2)
			*arrData=array('f1'=>'v1','f2'=>'v2')
			*/
		public function insert($table,$arrData=array()){
			if(!empty($arrData)){
				foreach($arrData as $k=>$v){
					$arrFields[]=$k;
					$arrValues[]="'".mysql_real_escape_string($v)."'";
				}
				$listFields=(is_array($arrFields)&&!empty($arrFields))?implode(',',$arrFields):'';
				$listValues=(is_array($arrValues)&&!empty($arrValues))?implode(',',$arrValues):'';
				if($listFields!=''&&$listValues!=''){
					$sql="INSERT INTO $table($listFields) VALUES($listValues)";
					if($this->execSQL($sql)){
						return 1;
					}
				}
				
			}
		}
		//UPDATE $TABLE SET F1=V1, F2=V2 WHERE $condition
		//Usefull MySQL DB
		public function update($table,$arrData=array(),$condition=''){
			if(!empty($arrData)){
				foreach($arrData as $k=>$v){
					$arrSET[]="`".$k."`='".mysql_real_escape_string($v)."'";
				}
				$listSET=(is_array($arrSET)&&!empty($arrSET))?implode(',',$arrSET):'';
				if($listSET!=''){
					if($condition!=''){
						$sql="UPDATE $table SET $listSET WHERE $condition";
					}else{
						$sql="UPDATE $table SET $listSET WHERE 1=1";
					}
					if($this->execSQL($sql)){
						return 1;
					}
				}
				
			}
		}
	}

?>