<?php
	function alert($mes){
		echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><script type='text/javascript'>alert('$mes')</script>";
	}
	function isEmail($email){
		$reg='/^[a-zA-Z0-9\_\-\.]+\@[a-zA-Z0-9]+\.[a-z]{2,4}$/';
		return preg_match($reg,$email);
	}
	function redir($url){
		if($url!=''){
			echo "<script language='javascript'>window.location.href='".$url."'</script>";
			exit();
		}
	}
	function getRandomString($len){
		$str="abcdefghijklm1234567890ABC@#";
		$L=strlen($str);
		$rStr='';
		for($i=0; $i<=$len; $i++){
			$rpos=rand(3,$L-1);
			$rStr.=$str[$rpos];
		}
		$rStr=md5($rStr);
		return $rStr;
	}
	function pagers($totalRecords,$limit,$limit_pagers='',$url='',$css){
		$totaPages=ceil($totalRecords/$limit);
		$pagers='';
		for($i=1; $i<=$totaPages; $i++){
			if($_GET['p']==$i){
				$css1=$css." active";
			}else{
				$css1=$css;
			};
			$pagers.="<a href='$url&p=$i' class='$css1' name='$i'>$i</a>";
		}
		
		return $pagers;
	}
        function getExt($file){
		$ext='';
		$posdotExt=strrpos($file,'.');
		if($posdotExt){
			$ext=substr($file,$posdotExt+1);
		}
		return strtolower($ext);
	}
	
	function uploadFile($file,$url,$allowType=array(),$maxSize=2048000){
		if($file!=''){
			$fName=$file['name'];
			$tmpfName=$file['tmp_name'];
			$fSize=$file['size'];
			$ext=getExt($fName);
			if($fSize>$maxSize){
				return '110i';
			}
			if(!in_array($ext,$allowType)){
				return '1200';
			}
			//rename file
			$fName=date('d-m-y-h-i-s').'_'.$fName;
			if(move_uploaded_file($tmpfName, $url.$fName)){
				return $fName;
			}
		}
	}
	function getCombobyArray($rs,$fkey,$fvalue,$boxName,$mes,$focus=null){
		$v1='';
		$select="<select name='$boxName'>
					<option value=''>$mes</option>";
		sort($rs);
                if($rs!='-1'){
                        foreach($rs as $row){
                                $select.="<option value='$row[$fkey]'>$row[$fvalue]</option>";
                        }
                }
		if($focus!=''){
			$select=str_replace("<option value='$focus'>","<option value='$focus' selected='selected'>",$select);
		}
		$select.='</select>';
		$v1=empty($_POST[$boxName])?'':$_POST[$boxName];
		if($v1!=''){
			$select=str_replace("<option value='$v1'>","<option value='$v1' selected='selected'>",$select);
		}
		return $select;
	}
	function getRemoteIPAddress(){
    $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
    return $ip;
	}
	
	function sendMail($fromname,$tomail,$subj,$body,$replyto=null){
	include("class.phpmailer.php");
	$mail			  = new PHPMailer();
	$mail->Mailer	  = "smtp";  
	$mail->CharSet    = 'utf-8';
	$mail->IsSMTP();
	$mail->FromName   = $fromname;
	$mail->SMTPAuth   = true;                 	 // enable SMTP authentication
	//$mail->SMTPSecure = "ssl"; 
	if($replyto!=null){
		$mail->AddReplyTo($replyto);                 // sets the prefix to the servier
	}
	$mail->Host       = "ssl://smtp.gmail.com";  // sets GMAIL as the SMTP server
	$mail->Port       = 465;                   	// set the SMTP port for the GMAIL server
	$mail->Username   = "task.ggg@gmail.com";  	// GMAIL username
	$mail->Password   = "1q2w3e4r5t#";     	// GMAIL password
	$mail->From       = "task.ggg@gmail.com";
	$mail->Subject    = $subj;
	$mail->WordWrap   = 5000; // set word wrap
	//send email
	$body= preg_replace('/\\\\/','', $body);
	$mail->MsgHTML($body);
	$mail->AddAddress($tomail);
	$mail->IsHTML(true);
	if($mail->Send()){
		return 1;
	}else{
		return 0;
	}
}
?>