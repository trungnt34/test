<?php
	global $dbs;
	global $baseUrl;
	global $appdb;	
	$id=$_GET['id'];
	$sqlKT="select TrangThai from tblvieclam
			where MaVL='$id'";
	$rsKT=$dbs->fetchOne($sqlKT);	
	if($rsKT[TrangThai]=="Đã đăng"){
		alert('Việc làm này đã đăng không được phép chỉnh sửa!');
		redir('?m=job&a=lst_job');
	}else{
		$errMes='';
		$tplEdit_job= new Xtemplate('views/jobs/edit.htm');
		$sql1="SELECT V.*,N.* FROM tblvieclam as V
				inner join tblnhatuyendung as N on N.MaNTD=V.MaNTD
				WHERE MaVL='$id'";
		$rs1=$dbs->fetchOne($sql1);
		
		$TenNTD=$rs1['TenNTD'];
		$LogoLink=$rs1['LogoLink'];
		$Website=$rs1['Website'];
		$QuyMo=$rs1['QuyMo'];
		$SoLuocVeNTD=$rs1['SoLuocVeNTD'];
		$MaNTD=$rs1['MaNTD'];
		
		
		
		$rs1=$dbs->fetchOne($sql1);
		$MaVL=$rs1['MaVL'];
		$TieuDe=$rs1['TieuDe'];
		$MaNganh=$rs1['MaNganh'];
		$MaTT=$rs1['MaTT'];
		$SoLuongTuyen=$rs1['SoLuongTuyen'];
		$LoaiVL=$rs1['LoaiVL'];
		$ViTri=$rs1['ViTri'];
		$MoTa=$rs1['MoTa'];
		$MucLuong=$rs1['MucLuong'];
		$DVTinhLuong=$rs1['DVTinhLuong'];
		$QuyenLoi=$rs1['QuyenLoi'];
		$YCBangCap=$rs1['YCBangCap'];
		$YCKiNangChuyenMon=$rs1['YCKiNangChuyenMon'];
		$YCKinhNghiem=$rs1['YCKinhNghiem'];
		$YCGioiTinh=$rs1['YCGioiTinh'];
		$YCDoTuoi=$rs1['YCDoTuoi'];
		$YCKhac=$rs1['YCKhac'];
		$YCHoSo=$rs1['YCHoSo'];
		$HanNopHS=$rs1['HanNopHS'];
		$TenNLH=$rs1['TenNLH'];
		$EmailNLH=$rs1['EmailNLH'];
		$DiaChiNLH=$rs1['DiaChiNLH'];
		$SoDT=$rs1['SoDT'];
		$SoNgayDang=$rs1['SoNgayDang'];
		
		
	
		$lstNganhNghe=$appdb->getComboBox('tblnganhnghe','MaNganh','TenNganh','cbxNganhNghe','...');
		
		$lstTinhThanh=$appdb->getComboBox('tbltinhthanh','MaTT','TenTT','cbxTinhThanh','...');
		
		if($_POST){
			//Tieu de
			$TieuDe=$_POST['txtTieuDe'];
			if(!$TieuDe){
				$errMes.="<li>- Tiêu đề việc làm không được để trống</li>";
				$do_save=-1;
			}
			
			//Mô tả công việc	
			$MaNganh=$_POST['cbxNganhNghe'];
			
			if(!$MaNganh){
				$errMes.="<li>- Hãy chọn ngành nghề</li>";
				$do_save=-1;
			}
			$MaTT=$_POST['cbxTinhThanh'];
		
			if(!$MaTT){
				$errMes.="<li>- Hãy chọn tỉnh thành</li>";
				$do_save=-1;
			}
			$SoLuongTuyen=$_POST['txtSoLuongTuyen'];
			if(!$SoLuongTuyen){
				$errMes.="<li>- Trường Số lượng tuyển không được phép để trống.</li>";
				$do_save=-1;
			}elseif(!preg_match("/^[0-9]+$/", $SoLuongTuyen)){
				$errMes.="<li>- Trường Số lượng tuyển phải là số nguyên.</li>";
				$do_save=-1;
			}
			$LoaiVL=$_POST['cbxLoaiVL'];
			if(!$LoaiVL){
				$errMes.="<li>- Trường Hãy chọn loại việc làm.</li>";
				$do_save=-1;
			}
			$ViTri=$_POST['txtViTri'];
			if(!$ViTri){
				$errMes.="<li>- Trường Vị trí không được phép để trống.</li>";
				$do_save=-1;
			}

			$MucLuong=$_POST['txtMucLuong'];
			$DVTinhLuong=$_POST['cbxDVTinhLuong'];
			if(($DVTinhLuong=="USD" or $DVTinhLuong=="VND")and !$MucLuong){
				$errMes.="<li>- Trường Mức lương không được phép để trống.</li>";
				$do_save=-1;
			}elseif(!preg_match("/^[0-9]+$/",$MucLuong) and $DVTinhLuong!='Thỏa thuận'){
				$errMes.="<li>- Trường Mức lương phải là số nguyên.</li>";
				$do_save=-1;
			}
			$MoTa=$_POST['txtaMoTa'];
			if(!$MoTa){
				$errMes.="<li>- Trường Mô tả không được phép để trống.</li>";
				$do_save=-1;
			}
			$QuyenLoi=$_POST['txtaQuyenLoi'];
			if(!$QuyenLoi){
				$errMes.="<li>- Trường Quyền lợi không được phép để trống.</li>";
				$do_save=-1;
			}
			
		//Yêu cầu
			$YCGioiTinh=$_POST['cbxYCGioiTinh'];
			if(!$YCGioiTinh){
				$errMes.="<li>- Trường Giới tính không được phép để trống.</li>";
				$do_save=-1;
			}
			$YCDoTuoi=$_POST['txtYCDoTuoi'];
			if(!$YCDoTuoi){
				$errMes.="<li>- Trường Tuổi không được phép để trống.</li>";
				$do_save=-1;
			}
			/*elseif(!preg_match("/^[0-9]+$/", $YCDoTuoi)){
				$errMes.="<li>- Tuổi phải là số nguyên.</li>";
				$do_save=-1;
			}elseif($YCDoTuoi<15 or ($YCDoTuoi>60 and $YCGioiTinh =="Nam") or ($YCDoTuoi>55 and $YCGioiTinh =="Nữ")){
				$errMes.="<li>- Tuổi phải nằm trong độ tuổi lao động do nhà nước quy định.</li>";
				$do_save=-1;
			}*/
			$YCBangCap=$_POST['cbxYCBangCap'];
			if(!$YCBangCap){
				$errMes.="<li>- Trường Trình độ không được phép để trống.</li>";
				$do_save=-1;
			}
			$YCKiNangChuyenMon=$_POST['txtaYCTrinhDo'];
			if(!$YCKiNangChuyenMon){
				$errMes.="<li>- Trường Kĩ năng không được phép để trống.</li>";
				$do_save=-1;
			}
			$YCKinhNghiem=$_POST['cbxYCKinhNghiem'];
			if(!$YCKinhNghiem){
				$errMes.="<li>- Trường Kinh nghiệm không được phép để trống.</li>";
				$do_save=-1;
			}
			$YCKhac=$_POST['txtaYCKhac'];
			
			
		//Yêu cầu hồ sơ
			$YCHoSo=$_POST['txtaYCHoSo'];
			if(!$YCKinhNghiem){
				$errMes.="<li>- Trường Hồ sơ bao gồm không được phép để trống.</li>";
				$do_save=-1;
			}	
			$ckHinhThucNop=$_POST['ckHinhThucNop']; 
			$HanNopHS=$_POST['txtHanNopHS'];
			if(!$HanNopHS){
				$errMes.="<li>- Trường Hạn nộp hồ sơ không được phép để trống.</li>";
				$do_save=-1;
			}elseif(strtotime($HanNopHS)<strtotime(date('d-m-Y'))){
				$errMes.="<li>- Hạn nộp hồ sơ phải lớn hơn ngày hiện tại.</li>";
				$do_save=-1;
			}
			
		//Thông tin liên hệ
			$TenNLH=$_POST['txtTenNLH'];
			if(!$TenNLH){
				$errMes.="<li>- Trường Tên người liên hệ không được phép để trống.</li>";
				$do_save=-1;
			}
			$EmailNLH=$_POST['txtEmailNLH'];
			if(!$EmailNLH){
				$errMes.="<li>- Trường E-Mail người liên hệ không được phép để trống.</li>";
				$do_save=-1;
			}elseif(!isEmail($EmailNLH)){
				$errMes.='<li>- Trường E-Mail người liên hệ không hợp lệ</li>';
				$do_save=-1;
			}
				
			
			$DiaChiNLH=$_POST['txtDiaChiNLH'];
			if(!$DiaChiNLH){
				$errMes.="<li>- Trường Địa chỉ người liên hệ không được phép để trống.</li>";
				$do_save=-1;
			}
			$SoDT=$_POST['txtSoDT'];
			if(!$SoDT){
				$errMes.="<li>- Trường Điện thoại liên hệ không được phép để trống.</li>";
				$do_save=-1;
			}elseif($SoDT&&!preg_match("/^[0-9]+$/",$SoDT)){
				$errMes.="<li>- Trường Điện thoại người liên hệ phải là số nguyên.</li>";
				$do_save=-1;
			}
			
		//Tùy chọn đăng
			$SoNgayDang=$_POST['cbxSoNgayDang'];
			if(!$SoNgayDang){
				$errMes.="<li>- Trường Số ngày đăng không được phép để trống.</li>";
				$do_save=-1;
			}
			
			if($errMes!=''){
				$tplEdit_job->assign('errMes',$errMes);
			}
		


		if($do_save!=-1){
			$HinhThucNop="Nộp trực tuyến|";
			if($ckHinhThucNop){
				foreach($ckHinhThucNop as $htn) {
					$HinhThucNop.=$htn."|";
				}
			}
			$arrData1=array('TieuDe'=>$TieuDe
							,'SoLuongTuyen'=>$SoLuongTuyen
							,'LoaiVL'=>$LoaiVL
							,'ViTri'=>$ViTri
							,'MucLuong'=>$MucLuong
							,'DVTinhLuong'=>$DVTinhLuong
							,'MoTa'=>$MoTa
							,'QuyenLoi'=>$QuyenLoi
							,'YCGioiTinh'=>$YCGioiTinh
							,'YCDoTuoi'=>$YCDoTuoi
							,'YCBangCap'=>$YCBangCap
							,'YCKiNangChuyenMon'=>$YCKiNangChuyenMon
							,'YCKinhNghiem'=>$YCKinhNghiem
							,'YCKhac'=>$YCKhac
							,'YCHoSo'=>$YCHoSo
							,'HinhThucNop'=>$HinhThucNop
							,'HanNopHS'=>date('Y-m-d',strtotime($HanNopHS))
							,'YCDoTuoi'=>$YCDoTuoi
							,'MaNTD'=>$MaNTD
							,'TenNLH'=>$TenNLH
							,'EmailNLH'=>$EmailNLH
							,'DiaChiNLH'=>$DiaChiNLH
							,'SoDT'=>$SoDT
							,'NgaySuaDoi'=>date('Y-m-d')
							,'MaTT'=>$MaTT
							,'SoNgayDang'=>$SoNgayDang
							,'MaNganh'=>$MaNganh);
				if($dbs->update('tblvieclam',$arrData1,"MaVL=$MaVL")){
				alert('Cập nhật hồ sơ thành công');
				redir('?m=job&a=lst_job');
				}
			}
		}
		//Gan nha tuyen dung
		$tplEdit_job->assign('baseUrl',$baseUrl);
		$tplEdit_job->assign('TenNTD',$TenNTD);
		if($rs['LogoLink']!=""){
			$tplEdit_job->assign('LogoLink',$LogoLink);
		}else{
			$tplEdit_job->assign('LogoLink','16-05-13-05-26-23_408407_311192022345168_1317613992_n.jpg');
		}
		
		$tplEdit_job->assign('QuyMo',$QuyMo);
		$tplEdit_job->assign('Website',$Website);
		$tplEdit_job->assign('SoLuocVeNTD',str_replace("\n","</br>",$SoLuocVeNTD));
		
		$tplEdit_job->assign('TieuDe',$TieuDe);
		$tplEdit_job->assign('MaNganh',$Nganh1);
		$tplEdit_job->assign('SoLuongTuyen',$SoLuongTuyen);
		$tplEdit_job->assign('LoaiVL',$LoaiVL);
		$tplEdit_job->assign('ViTri',$ViTri);
		$tplEdit_job->assign('MucLuong',$MucLuong);
		$tplEdit_job->assign('DVTinhLuong',$DVTinhLuong);
		$tplEdit_job->assign('MoTa',$MoTa);
		$tplEdit_job->assign('QuyenLoi',$QuyenLoi);
		//Yêu cầu
		$tplEdit_job->assign('YCGioiTinh',$YCGioiTinh);
		$tplEdit_job->assign('YCDoTuoi',$YCDoTuoi);
		$tplEdit_job->assign('YCBangCap',$YCBangCap);
		$tplEdit_job->assign('YCKiNangChuyenMon',$YCKiNangChuyenMon);
		$tplEdit_job->assign('YCKinhNghiem',$YCKinhNghiem);
		$tplEdit_job->assign('YCKhac',$YCKhac);
		//Yêu cầu hồ sơ
		$tplEdit_job->assign('YCHoSo',$YCHoSo);
		$tplEdit_job->assign('HanNopHS',$HanNopHS);
		//Thông tin liên hệ
		$tplEdit_job->assign('TenNLH',$TenNLH);
		$tplEdit_job->assign('EmailNLH',$EmailNLH);
		$tplEdit_job->assign('DiaChiNLH',$DiaChiNLH);
		$tplEdit_job->assign('SoDT',$SoDT);
		$tplEdit_job->assign('SoNgayDang',$SoNgayDang);
		
		$tplEdit_job->assign('lstNganhNghe',$lstNganhNghe);
		$tplEdit_job->assign('lstTinhThanh',$lstTinhThanh);
		$tplEdit_job->parse('EDIT_JOB');
		$acontents= $tplEdit_job->text('EDIT_JOB');
	}
?>
