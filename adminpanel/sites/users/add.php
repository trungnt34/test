<?php
	$tplAdd=new XTemplate('views/users/add.htm');
	$errMes='';
	$do_save=0;
	global $dbs;

	if($_POST){
		$TenNQT=$_POST['txtTenNQT'];
		$Email=$_POST['txtEmail'];
		$password=$_POST['txtPassword'];
		$cpwd=$_POST['txtConfirmPassword'];
		$DienThoai=$_POST['txtDienThoai'];
		$Quyen=$_POST['cbxQuyen'];
		
		if(!$TenNQT){
			$errMes.="<li>Họ và tên không được để trống</li>";
			$do_save=-1;
		}
		if(!$Email){
			$errMes.="<li>Email không được để trống </li>";
			$do_save=-1;
		}else if(!isEmail($Email)){
			$errMes.="<li> Email không đúng</li>";
			$do_save=-1;
		}
		if(!$password){
			$errMes.="<li>Mật khẩu không được để trống</li>";
			$do_save=-1;
		}
		if(!$cpwd){
			$errMes.="<li>Trường nhập lai mật khẩu không được để trống</li>";
			$do_save=-1;
		}else if($password !=$cpwd){
			$errMes.="<li>Mật khẩu nhập lại không trùng khớp</li>";
			$do_save=-1;
		}
		if(!$DienThoai){
			$errMes.="<li>Điện thoại không được để trống</li>";
			$do_save=-1;
		}elseif(!preg_match("/^[0-9]+$/", $DienThoai)){
			$errMes.="<li>Điện thoại phải là kí tự số</li>";
			$do_save=-1;
		}
		if(!$Quyen){
			$errMes.="<li>Quyền không được để trống</li>";
			$do_save=-1;
		}
		//}else if(!isEmail($usr_email)){ 
//			$errMes.="<li>Email invalid</li>";
//			$do_save=-1;

		if($errMes!=''){
			$tplAdd->assign('errMes',$errMes);
		}
		if($do_save!=-1){
			$arrData=array('Email'=>$Email
						   ,'Pwd'=>$password
						   ,'TenNQT'=>$TenNQT
						   ,'DienThoai'=>$DienThoai
						   ,'Quyen'=>$Quyen);
			if($dbs->insert('tbluser',$arrData)){
				redir("?m=users&a=list");
			}
		}
		$tplAdd->assign('Email',$Email);
		$tplAdd->assign('TenNQT',$TenNQT);
		$tplAdd->assign('DienThoai',$DienThoai);
		$tplAdd->assign('Quyen',$Quyen);
		
	}
	$tplAdd->parse('ADD');
	$acontents=$tplAdd->text('ADD');
?>