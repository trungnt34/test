<?php
	$tpledit=new XTemplate('views/users/edit.htm');
	$errMes='';
	$do_save=0;
	global $dbs;
	$id=$_GET['id'];
	$sql="SELECT * FROM tbluser WHERE id=$id";
	$rs=$dbs->fetchOne($sql);
	$Email=$rs['Email'];
	$TenNQT=$rs['TenNQT'];
	$password=$rs['Pwd'];
	$cpwd=$rs['Pwd'];
	$DienThoai=$rs['DienThoai'];
	$Quyen=$rs['Quyen'];
	$tpledit->assign('cpwd',$cpwd);
	$tpledit->assign('password',$password);
	if($_POST){
		$TenNQT=$_POST['txtTenNQT'];
		$Email=$_POST['txtEmail'];
		$password=$_POST['txtPassword'];
		$cpwd=$_POST['txtConfirmPassword'];
		$DienThoai=$_POST['txtDienThoai'];
		$Quyen=$_POST['cbxQuyen'];
		
		if(!$TenNQT){
			$errMes.="<li>Họ và tên không được để trống</li>";
			$do_save=-1;
		}
		if(!$Email){
			$errMes.="<li>Email không được để trống </li>";
			$do_save=-1;
		}elseif(!isEmail($Email)){
			$errMes.="<li> Email không đúng</li>";
			$do_save=-1;
		}
		if(!$password){
			$errMes.="<li>Mật khẩu không được để trống</li>";
			$do_save=-1;
		}
		if(!$cpwd){
			$errMes.="<li>Trường nhập lai mật khẩu không được để trống</li>";
			$do_save=-1;
		}else if($password !=$cpwd){
			$errMes.="<li>Mật khẩu nhập lại không trùng khớp</li>";
			$do_save=-1;
		}
		if(!$DienThoai){
			$errMes.="<li>Điện thoại không được để trống</li>";
			$do_save=-1;
		}elseif(!preg_match("/^[0-9]+$/", $DienThoai)){
			$errMes.="<li>Điện thoại phải là kí tự số</li>";
			$do_save=-1;
		}
		if(!$Quyen){
			$errMes.="<li>Quyền không được để trống</li>";
			$do_save=-1;
		}
		//}else if(!isEmail($usr_email)){ 
//			$errMes.="<li>Email invalid</li>";
//			$do_save=-1;

		if($errMes!=''){
			$tpledit->assign('errMes',$errMes);
		}
		if($do_save!=-1){
			$arrData=array('Email'=>$Email
						   ,'Pwd'=>$password
						   ,'TenNQT'=>$TenNQT
						   ,'DienThoai'=>$DienThoai
						   ,'Quyen'=>$Quyen);
			if($dbs->update('tbluser',$arrData,"id=$id")){
				redir("?m=users&a=list");
			}
		}
		
	}
	$tpledit->assign('Email',$Email);
	$tpledit->assign('TenNQT',$TenNQT);
	$tpledit->assign('DienThoai',$DienThoai);
	$tpledit->assign('Quyen',$Quyen);
	$tpledit->parse('EDIT');
	$acontents=$tpledit->text('EDIT');
?>
