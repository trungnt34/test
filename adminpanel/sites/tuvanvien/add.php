<?php
	$tplAdd=new XTemplate('views/tuvanvien/add.htm');
	$errMes='';
	$do_save=0;
	global $dbs;
	if($_POST){
		$TenTVV=$_POST['TenTVV'];
		$email=$_POST['email'];
		$yahoo=$_POST['yahoo'];
		$skype=$_POST['skype'];
		$Dienthoai=$_POST['Dienthoai'];
		$NoiHoatDong=$_POST['NoiHoatDong'];
		$TrangThai=$_POST['TrangThai'];
		if(!$TenTVV){
			$errMes.="<li>Họ và tên không được để trống</li>";
			$do_save=-1;
		}
		if(!$email){
			$errMes.="<li>Email không được để trống</li>";
			$do_save=-1;
		}else if(!isEmail($email)){ 
			$errMes.="<li>Email không đúng định dạng</li>";
			$do_save=-1;
		}
		if(!$yahoo){
			$errMes.="<li>Tài khoản yahoo không được để trống</li>";
			$do_save=-1;
		}
		if(!$skype){
			$errMes.="<li>Tài khoản skype không được để trống</li>";
			$do_save=-1;
		}if(!$Dienthoai){
			$errMes.="<li>Điện thoại không được để trống</li>";
			$do_save=-1;
		}elseif(!preg_match("/^[0-9]+$/",$Dienthoai)){
			$errMes.="<li>Điện thoại phải là số</li>";
			$do_save=-1;
		}
		if(!$NoiHoatDong){
			$errMes.="<li>Trang hoạt động không được để trống</li>";
			$do_save=-1;
		}
		if(!$TrangThai){
			$errMes.="<li>Trạng thái không được để trống</li>";
			$do_save=-1;
		}
		
		
		
		if($errMes!=''){
			$tplAdd->assign('errMes',$errMes);
		}
		if($do_save!=-1){
		
		$arrData=array('TenTVV'=>$TenTVV
					   ,'email'=>$email
					    ,'yahoo'=>$yahoo
						 ,'skype'=>$skype
						  ,'Dienthoai'=>$Dienthoai
						  ,'NoiHoatDong'=>$NoiHoatDong
						  ,'TrangThai'=>$TrangThai);
		
		if($dbs->insert('tbltuvanvien',$arrData)){
			redir("?m=tuvanvien&a=list");
			}
		}
		$tplAdd->assign('TenTVV',$TenTVV);
		$tplAdd->assign('email',$email);
		$tplAdd->assign('yahoo',$yahoo);
		$tplAdd->assign('skype',$skype);
		$tplAdd->assign('Dienthoai',$Dienthoai);
		$tplAdd->assign('NoiHoatDong',$NoiHoatDong);
		$tplAdd->assign('TrangThai',$TrangThai);
	}
	$tplAdd->parse('ADD');
	$acontents=$tplAdd->text('ADD');
?>