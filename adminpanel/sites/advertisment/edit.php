<?php
	/*$rolList=$_SESSION['rol_list'];
	$ma=$m.'|'.$a;
	$rl=checkRoles($ma,$rolList);
	if($rl==false){
		alert('You are not Permision');
		redir('index.php');
	}*/

	global $dbs;
	global $appdb;
	global $baseUrl;
	
	$tplEdit=new XTemplate('views/advertisment/edit.htm');
	$errMes='';
	$do_save=0;
	
	$id=$_GET['id'];
	$sql="SELECT * FROM advertising WHERE id=$id";
	$rs=$dbs->fetchOne($sql);
	
	$title=$rs['title'];
	$image=$rs['image'];
	$description=$rs['description'];
	$link=$rs['link'];
	
/*	//Create 2 array to load ComboBoxbyArray
	$arrTarget=array('_blank'=>'_blank','_self'=>'_self');
	$arrStatus=array('active'=>'active','deactive'=>'deactive');
	//Load ComboBox
	$select=$appdb->getComboBox('tblposition_adv','position_id','position_name','positionid','Please choose Position',$position_id);
	$listTarget=getCombobyArray($arrTarget,'txtadv_target','Please choose Target',$focus_target);
	$listStatus=getCombobyArray($arrStatus,'txt_status','Please choose Status',$focus_statue);*/
	
	$url='./img/advertising/';
	$allowType=array('jpg','png','jpeg');
	$maxSize=1024000;
	
	
	if($_POST){
		$title=$_POST['txtTile'];	
		$description=$_POST['txtDescription'];
		$link=$_POST['txtLink'];
		$file=!empty($_FILES['txtfile'])?$_FILES['txtfile']:'';
		$adv_image_new=uploadFile($file, $url, $allowType, $maxSize);
	
		if(!$title){
			$errMes.='<li>Tiêu đề không được để trống</li>';
			$do_save=-1;	
		}
		if(!$description){
			$errMes.='<li>Mô tả không được để trống/li>';
			$do_save=-1;	
		}
		if(!$link){
			$errMes.='<li>Link không được để trống</li>';
			$do_save=-1;	
		}
		
		if($errMes!=''){
			$tplEdit->assign('errMes',$errMes);
		}
		
		if($file['name']!=''){
			if($adv_image_new=='110i'){
				alert('File size not allow');
			}
			if($adv_image_new=='1200'){
				alert('File type not allow');
			}
		}
		
		if($do_save!=-1){
			if($file['name']!=''){
				if($adv_image_new!='110i' && $adv_image_new!='1200'){
					$adv_image_old=$image;
					$image=$adv_image_new;
					@unlink("img/advertising/$adv_image_old");
				}
			}else{
				$image=$image;
			}
		$arrData=array('title'=>$title,'image'=>$image,'link'=>$link,'description'=>$description);	
			if($dbs->update('advertising',$arrData,"id=$id")){
				redir('?m=advertisment&a=list');	
			}
		}	
	}
	
	$tplEdit->assign('baseUrl',$baseUrl);
	$tplEdit->assign('title',$title);
	$tplEdit->assign('image',$image);
	$tplEdit->assign('link',$link);
	$tplEdit->assign('description',$description);
	$tplEdit->parse('EDIT');
	$acontents=$tplEdit->text('EDIT');


?>
