-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 02, 2013 at 05:09 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `myjob`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblbangcap`
--

CREATE TABLE IF NOT EXISTS `tblbangcap` (
  `MaBC` int(11) NOT NULL AUTO_INCREMENT,
  `LoaiBC` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `NoiDaoTao` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ChuyenNganh` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `NgayBatDau` date NOT NULL,
  `NgayKetThuc` date NOT NULL,
  `XepLoai` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `MoTa` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `MaHS` int(11) NOT NULL,
  PRIMARY KEY (`MaBC`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tblbangcap`
--

INSERT INTO `tblbangcap` (`MaBC`, `LoaiBC`, `NoiDaoTao`, `ChuyenNganh`, `NgayBatDau`, `NgayKetThuc`, `XepLoai`, `MoTa`, `MaHS`) VALUES
(1, 'Kỹ sư', 'Trường Đại học Nông Nghiệp Hà Nội', 'Tin học', '2008-01-01', '2013-01-01', 'Trung bình', 'Tham gia một số hoạt động tình nguyện và các câu lạc bộ do Khoa Công nghệ thông tin tổ chức!', 1),
(11, 'Trung học', 'ĐH Nông nghiệp', 'Tin học', '1997-02-01', '2003-02-01', 'Trung bình', 'Nhiệt tình tham gia các hoạt động ngoại khóa.', 21);

-- --------------------------------------------------------

--
-- Table structure for table `tblcounter`
--

CREATE TABLE IF NOT EXISTS `tblcounter` (
  `id_counter` int(11) NOT NULL AUTO_INCREMENT,
  `MaVL` int(11) NOT NULL,
  `MaHS` int(11) NOT NULL,
  `IP` varchar(11) NOT NULL,
  PRIMARY KEY (`id_counter`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tblcounter`
--

INSERT INTO `tblcounter` (`id_counter`, `MaVL`, `MaHS`, `IP`) VALUES
(7, 0, 14, '127.0.0.1'),
(8, 0, 13, '127.0.0.1'),
(9, 0, 14, '127.0.0.2'),
(10, 0, 14, '127.0.0.3'),
(11, 0, 1, '127.0.0.1'),
(12, 0, 21, '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `tblhoso`
--

CREATE TABLE IF NOT EXISTS `tblhoso` (
  `MaHS` int(11) NOT NULL AUTO_INCREMENT,
  `TieuDe` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `KiNangChuyenMon` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `KiNangKhac` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `LuotXem` int(11) NOT NULL,
  `TrangThai` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `NgaySuaDoi` date NOT NULL,
  `NgayDang` date NOT NULL,
  `MaNTV` int(11) NOT NULL,
  `MucLuong` int(11) DEFAULT NULL,
  `DVTinhLuong` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ViTriMM` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LoaiCV` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MucTieuNgheNghiep` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `DiCongTac` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `MaNganh` int(11) NOT NULL,
  `MaTT` int(11) NOT NULL,
  PRIMARY KEY (`MaHS`),
  KEY `MaNTV` (`MaNTV`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `tblhoso`
--

INSERT INTO `tblhoso` (`MaHS`, `TieuDe`, `KiNangChuyenMon`, `KiNangKhac`, `LuotXem`, `TrangThai`, `NgaySuaDoi`, `NgayDang`, `MaNTV`, `MucLuong`, `DVTinhLuong`, `ViTriMM`, `LoaiCV`, `MucTieuNgheNghiep`, `DiCongTac`, `MaNganh`, `MaTT`) VALUES
(1, 'Lập trình viên PHP&MySQL', '-Lập trình tốt với các ngôn ngữ PHP&MySQL , VB.NET, ,ASP.NET.\r\n-Sử dụng thành thạo các hệ quản trị cơ sở dữ liệu: MySQL, SQL Server...\r\n-Có khả năng tư duy phân tích thiết kế hệ thống.', '-Có khả năng làm việc theo nhóm hoặc làm việc độc lập!\r\n-Kỹ năng giao tiếp tốt.\r\n-Tận tụy với công việc.', 1, 'Đã đăng', '2013-06-10', '2013-06-09', 16, 5000000, 'VND', 'Nhân viên', 'Sinh viên thực tập', 'Tích lũy được nhiều kinh nghiệm tạo tiền đề phát triển tương lại cho bản thân Mong muốn tạo được doanh thu cao cho Công ty khi được làm việc/', 'Có', 8, 1),
(12, 'Kỹ sư xây dựng', '1. Xây dựng các kế hoạch nhằm thực hiện nhiệm vụ : Kế hoạch vật tư, thiết bị, lao động, tiền vốn, kế hoạch sản lượng. 2. Quản lý, sử dụng các nguồn lực để thực hiện nhiệm vụ sản xuất. Điều hành quyết định tất cả các vấn đề có liên quan đến hoạt động hàng ngày của công trường qua ban chỉ huy công trường và các phòng ban. 3. Tham dự các cuộc họp giao ban, kiểm tra, xét duyệt biện pháp thi công phù hợp với từng công trình, điều kiện của công ty. 4. Phối hợp nhịp nhàng với các phòng ban Công ty trên mọi mặt có liên quan đến việc triển khai thi công.  5. Kiểm tra, tổng hợp khối lượng thanh toán các giai đoạn theo hợp đồng cũng như thanh toán các thầu phụ.  6. Kiểm tra, đôn đốc ban chỉ huy công trình thực hiện các mục tiêu tiến độ, mục tiêu chất lượng, biện pháp thi công đã đề ra. 7. Chịu trách nhiệm về định mức vật tư đã thực hiện khi lập dự toán thi công, kiểm soát khối lượng vật tư khi thiết kế thay đổi. 8. Triển khai, chủ trí công tác đấu thầu về khối lượng, đơn giá, biện pháp, tính hiệu', 'Nhanh nhẹn, quyết đoán và có tầm nhìn về công việc mình đang làm, phụ trách.', 24, 'Đang chờ', '2013-06-10', '0000-00-00', 17, 10000000, 'VND', 'Nhân viên', 'Toàn thời gian cố định', 'Phấn đầu làm việc để hoàn thiện bản thân để trở thành một chuyên viên ngành xây dựng mà mình yêu thích.', 'Có', 8, 1),
(13, 'Biên tập viên', '- Có hơn 05 năm kinh nghiệm trong lĩnh vực truyền thông.( Báo hình + báo mạng ) - Có kỹ năng quản lý và tổ chức công việc tốt, định hướng công việc theo kết quả.  - Có khả năng làm việc linh hoạt với nhiều đối tượng khách hàng khách nhau để phục vụ cho chương trình truyền hình.', '- Có khả năng chịu áp lực công việc cao.  - Có kỹ năng đàm phán, thương lượng phục vụ cho các chương trình truyền hình. - Có mối quan hệ tốt với công ty truyền thông, các agency và các đài truyền hình, đặc biệt là HTV và VTV và các đài truyền hình địa phương.', 1, 'Đã đăng', '2013-06-10', '0000-00-00', 18, 5000000, 'VND', 'Nhân viên', 'Toàn thời gian cố định', '', 'Có', 2, 1),
(14, 'Nhân viên IT- Triển khai phần mềm', 'Cài đặt phần mềm cho hệ thống máy tính của các Công ty, cơ quan...', 'Nhiệt tình, có thể làm việc trong môi trường cạnh tranh', 3, 'Đã đăng', '2013-06-10', '2013-06-17', 19, NULL, 'Thỏa thuận', 'Nhân viên', 'Bán thời gian cố định', 'Tích lũy được nhiều kinh nghiệm tạo tiền đề phát triển tương lại cho bản thân Mong muốn tạo được doanh thu cao cho Công ty khi được làm việc/', 'Có', 8, 1),
(15, 'Kế toán trưởng / Kế toán tổng hợp', 'Xây dựng bộ máy kế toán – Set toàn bộ hệ thống kế toán ổn định - Có khả năng thu hồi các khoản công nợ khó đòi, bị chiếm dụng - Kiểm tra kiểm soát chặt chẽ các khoản chi phí đem lại hiệu quả cao cho Công ty.', '- Hoạt bát, năng động trong công việc và có khả năng làm việc độc lập. - Không ngừng học tập, nâng cao tri thức, học hỏi kinh nghiệm trên mọi lĩnh vực và sẵn sàng nắm bắt mọi cơ hội để phát triển sự nghiệp.', 32, 'Đang chờ', '2013-06-10', '2013-06-17', 21, 7000000, 'VND', 'Trưởng/Phó phòng', 'Toàn thời gian cố định', 'Tôi có thể sử dụng các kinh nghiệm và phát huy hết khả năng của mình để quản lý, điều hành, kiểm soát tốt hàng hóa, tài sản và bộ máy kế toán, ... để trở thành một Giám đốc Tài Chính hoặc CEO chuyên nghiệp. Phương chăm làm việc: "Có niềm đam mê trong công việc thì chắc chắn sẽ công"', 'Có', 8, 3),
(16, 'Nhân viên quản trị Mang', '', '', 0, '', '0000-00-00', '0000-00-00', 22, NULL, '', NULL, NULL, '', '', 8, 1),
(17, 'Lập trình viên lập trình C++/ C# / .Net / Java', '', '', 8, '', '0000-00-00', '0000-00-00', 20, NULL, '', NULL, NULL, '', '', 8, 2),
(18, 'Nhân viên Lập Trình web - webdesign', 'Thành thạo HTML,CSS, Javascrips -Thành thạo các frameword Zend, Yii, Cake', 'Nhanh nhẹn, hoạt bát', 42, 'Đang chờ', '2013-06-10', '2013-06-10', 21, NULL, 'Thỏa thuận', 'Nhân viên', 'Bán thời gian cố định', 'sdfasdfasdf', 'Có', 8, 3),
(19, 'Lập Trình Viên PHP (làm part Time)', '', '', 0, '', '0000-00-00', '0000-00-00', 23, NULL, '', NULL, NULL, '', '', 8, 5),
(20, 'Lập trình viên C#, ASP.NET, C++', '', '', 0, '', '0000-00-00', '0000-00-00', 24, NULL, '', NULL, NULL, '', '', 8, 17),
(21, 'Kỹ sư tin học', '- Thành thạo tin học\r\n- Có khả năng phân tích thiết kế hệ thống.\r\n- Sử dụng thành thạo các hệ quản trị dữ liệu MySQL, SQLServer', '-Chăm chỉ, cần cù, siêng năng\r\n-Tiếp thu nhanh', 1, 'Đã đăng', '2013-06-10', '2013-06-17', 16, 5000000, 'VND', 'Nhân viên', 'Sinh viên thực tập', 'Được học hỏi, thăng tiến', 'Có', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblhosodaluu`
--

CREATE TABLE IF NOT EXISTS `tblhosodaluu` (
  `MaHSDL` int(11) NOT NULL AUTO_INCREMENT,
  `MaHS` int(11) NOT NULL,
  `MaNTD` int(11) NOT NULL,
  PRIMARY KEY (`MaHSDL`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tblhosodaluu`
--

INSERT INTO `tblhosodaluu` (`MaHSDL`, `MaHS`, `MaNTD`) VALUES
(12, 15, 4),
(13, 1, 4),
(18, 21, 4),
(9, 14, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tblhoso_vieclam`
--

CREATE TABLE IF NOT EXISTS `tblhoso_vieclam` (
  `MaHS_VL` int(11) NOT NULL AUTO_INCREMENT,
  `MaHS` int(11) NOT NULL,
  `MaVL` int(11) NOT NULL,
  `TrangThai` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaHS_VL`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tblhoso_vieclam`
--

INSERT INTO `tblhoso_vieclam` (`MaHS_VL`, `MaHS`, `MaVL`, `TrangThai`) VALUES
(1, 1, 28, ''),
(6, 12, 28, ''),
(7, 1, 29, '');

-- --------------------------------------------------------

--
-- Table structure for table `tblkinhnghiem`
--

CREATE TABLE IF NOT EXISTS `tblkinhnghiem` (
  `MaKN` int(11) NOT NULL AUTO_INCREMENT,
  `CongTy` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ViTri` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ThoiGianLamViec` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NhiemVuChinh` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ThanhTich` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `MaHS` int(11) NOT NULL,
  PRIMARY KEY (`MaKN`),
  KEY `NhiemVuChinh` (`NhiemVuChinh`),
  KEY `ViTri` (`ViTri`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tblkinhnghiem`
--

INSERT INTO `tblkinhnghiem` (`MaKN`, `CongTy`, `ViTri`, `ThoiGianLamViec`, `NhiemVuChinh`, `ThanhTich`, `MaHS`) VALUES
(1, 'FPT', 'Nhân viên', '1 năm', 'Cài đặt các dự án của công ty', '-Nhân viên ưu tú  ^_^', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblnganhnghe`
--

CREATE TABLE IF NOT EXISTS `tblnganhnghe` (
  `MaNganh` int(11) NOT NULL AUTO_INCREMENT,
  `TenNganh` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaNganh`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=44 ;

--
-- Dumping data for table `tblnganhnghe`
--

INSERT INTO `tblnganhnghe` (`MaNganh`, `TenNganh`) VALUES
(1, 'Bán hàng'),
(2, 'Báo chí/ Biên tập viên/ Xuất bản'),
(3, 'Bảo hiểm'),
(4, 'Bất động sản'),
(5, 'Biên phiên dịch/ Thông dịch viên'),
(6, 'Chăm sóc sức khỏe/ Y tế'),
(7, 'CNTT - Phần cứng/ Mạng'),
(8, 'CNTT - Phần mềm'),
(9, 'Dầu khí/ Khoáng sản'),
(10, 'Dệt may/ Da giày'),
(11, 'Dịch vụ khách hàng'),
(12, 'Dược/ Sinh học'),
(13, 'Điện/ Điện tử'),
(14, 'Giáo dục/ Đào tạo/ Thư viện'),
(15, 'Hóa chất/ Sinh hóa/ Thực phẩm'),
(16, 'Kế toán/ Tài chính/ Kiểm toán'),
(17, 'Khách sạn/ Du lịch'),
(18, 'Kiến trúc '),
(19, 'Kỹ thuật ứng dụng/ Cơ khí'),
(20, 'Lao động phổ thông'),
(21, 'Môi trường/ Xử lý chất thải'),
(22, 'Mới tốt nghiệp/ Thực tập'),
(23, 'Ngân hàng/ Chứng khoán/ Đầu tư'),
(24, 'Nghệ thuật/ Thiết kế/ Giải trí'),
(25, 'Nhà hàng/ Dịch vụ ăn uống'),
(26, 'Nhân sự '),
(27, 'Nông nghiệp/ Lâm nghiệp'),
(28, 'Ô tô'),
(29, 'Pháp lý/ Luật'),
(30, 'Phi chính phủ/ Phi lợi nhuận'),
(31, 'Quản lý chất lượng (QA/ QC)'),
(32, 'Quản lý điều hành'),
(33, 'Quảng cáo/ Khuyến mãi'),
(35, 'Thư ký/ Hành chính'),
(36, 'Tiếp thị '),
(37, 'Tư vấn'),
(38, 'Vận chuyển/ Giao thông/ Kho bãi'),
(39, 'Vật tư/ Mua hàng'),
(40, 'Viễn Thông'),
(41, 'Xây dựng'),
(42, 'Xuất nhập khẩu/ Ngoại thương'),
(43, 'Khác ');

-- --------------------------------------------------------

--
-- Table structure for table `tblnguoitimviec`
--

CREATE TABLE IF NOT EXISTS `tblnguoitimviec` (
  `MaNTV` int(11) NOT NULL AUTO_INCREMENT,
  `TenNTV` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NgaySinh` date NOT NULL,
  `GioiTinh` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `LinkAnhDaiDien` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TinhTrangHonNhan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DiaChi` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DTLienHe` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Pwd` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaNTV`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `tblnguoitimviec`
--

INSERT INTO `tblnguoitimviec` (`MaNTV`, `TenNTV`, `NgaySinh`, `GioiTinh`, `LinkAnhDaiDien`, `TinhTrangHonNhan`, `DiaChi`, `DTLienHe`, `Email`, `Pwd`) VALUES
(16, 'Nguyễn Thành Trung', '1989-04-03', 'Nam', '', 'Độc thân', 'Trác Văn - Duy Tiên - Hà Nam', '01655052774', 'nguyenthanhtrung.dthn@gmail.com', '123456'),
(17, 'Lê Thị Phương Lan', '0000-00-00', '', '', '', '', '', '', ''),
(18, 'Nguyễn Văn Thiện', '0000-00-00', '', '', '', '', '', '', ''),
(19, 'Phạm Văn Quang', '0000-00-00', '', '', '', '', '', '', ''),
(20, 'Cao Thị Châu', '0000-00-00', '', '', '', '', '', '', ''),
(21, 'Nguyễn Thanh Thủy', '0000-00-00', '', '', '', '', '', '', ''),
(22, 'Hồ Quỳnh Hương', '0000-00-00', '', '', '', '', '', '', ''),
(23, 'Nguyễn Văn Lực', '0000-00-00', '', '', '', '', '', '', ''),
(24, 'Nguyễ Như Lê', '0000-00-00', '', '', '', '', '', '', ''),
(25, 'Hoàng Thị Huệ', '0000-00-00', '', '', '', '', '', '', ''),
(26, 'Nguyễn Công Lưu', '0000-00-00', '', '', '', '', '', '', ''),
(31, 'Hoàng Hà', '1980-02-16', 'Nữ', '01-06-13-06-49-46_3631722-dragon-tattoo-tribal-vector-illustration.jpg', 'Đã kết hôn', 'Dường T-ĐH Nông nghiệp Hà Nội', '0936856854', 'hahtus@gmail.com', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `tblnhatuyendung`
--

CREATE TABLE IF NOT EXISTS `tblnhatuyendung` (
  `MaNTD` int(11) NOT NULL AUTO_INCREMENT,
  `TenNTD` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `DiaChi` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `DienThoaiLH` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Pwd` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Website` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `LogoLink` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `SoLuocVeNTD` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `QuyMo` int(10) NOT NULL,
  PRIMARY KEY (`MaNTD`),
  KEY `Website` (`Website`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tblnhatuyendung`
--

INSERT INTO `tblnhatuyendung` (`MaNTD`, `TenNTD`, `DiaChi`, `DienThoaiLH`, `Email`, `Pwd`, `Website`, `LogoLink`, `SoLuocVeNTD`, `QuyMo`) VALUES
(4, 'Công Ty TNHH Estelle Việt Nam', 'Dường 1-Cửu Việt-Trâu Quỳ-Gia Lâm-Hà Nội', '1263494', 'nguyenthanhtrung.dthn@gmail.com', '123456', 'estelle.com.vn', '16-05-13-05-26-23_408407_311192022345168_1317613992_n.jpg', '- Thiết kế website chuyên nghiệp theo yêu cầu của tổ chức cá nhân.\r\n\r\n- Cung cấp domain - Hosting cho website.\r\n\r\n- Đào tạo, tập huấn triển khai các chương trình giáo dục và ứng dụng CNTT trong giáo dục, học tập và làm việc\r\n\r\n- Phát triển phần mềm ứng dụng theo yêu cầu từng cơ quan, doanh nghiệp, tổ chức, cửa hàng...\r\n\r\n- Phát triển phần mềm ứng dụng cho mobile: Android và iOS\r\n\r\n- Thiết kế quảng cáo, card visit, băng rôn, áp phích, khung chữ, danh thiếp, In ấn, scan, in phun khổ lớn.\r\n\r\n- Thiết kế, lắp đặt và cấu hình mạng cho doanh nghiệp, cơ quan, tổ chức, cửa hàng internet...\r\n\r\n- Sửa chữa, cài đặt, bảo trì, cứu dữ liệu, diệt virus PC, Laptop.\r\n\r\n- Cung cấp thiết bị, phụ kiện PC, laptop.', 500),
(5, 'Công Ty Cổ Phần Giải Pháp Phần Mềm Hanel', '', '0', 'nhansu@hanelsoft.vn', '123456', 'hanelsoft.vn', '', '', 0),
(6, 'One Corp', '', '0', 'luan.trieu@one.com', '', 'www.one.com.vn', '', '', 0),
(7, 'Tổng Công Ty Thương Mại Sài Gòn', '', '0', '', '', '', '', '', 0),
(8, 'Công Ty TNHH Estelle Việt Nam', '', '0', '', '', '', '', '', 0),
(9, 'CTy CP Đất Xanh Đông Á', '', '0', '', '', '', '', '', 0),
(10, 'Tổ Hợp Giáo Dục TOPICA', '', '0', '', '', '', '', '', 0),
(11, 'TNHH dịch vụ thiết kế web', 'Dường T-ĐH Nông nghiệp Hà Nội', '934856859', 'nvthien55a@gmail.com', '123456', 'hua.edu.vn', '01-06-13-05-19-35_1362884505tuyendung.jpg', '<p>C&ocirc;ng ty chuy&ecirc;n thiết kế web theo y&ecirc;u cầu</p>\r\n', 25),
(13, 'TNHH dịch vụ thiết kế web', 'Dường T-ĐH Nông nghiệp Hà Nội', '0934856859', 'hahtus@gmail.com', '123456', 'hua.edu.vn', '01-06-13-06-11-21_3631722-dragon-tattoo-tribal-vector-illustration.jpg', '<p>Chuy&ecirc;n cung cấp c&aacute;c dịch vụ host, domain</p>\r\n\r\n<p>Thiết kế Web theo y&ecirc;u cầu</p>\r\n', 20);

-- --------------------------------------------------------

--
-- Table structure for table `tblthongbao`
--

CREATE TABLE IF NOT EXISTS `tblthongbao` (
  `MaTB` int(11) NOT NULL AUTO_INCREMENT,
  `TieuDe` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `NoiDung` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `MaNTD` int(11) NOT NULL,
  PRIMARY KEY (`MaTB`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tblthongbao`
--

INSERT INTO `tblthongbao` (`MaTB`, `TieuDe`, `NoiDung`, `MaNTD`) VALUES
(4, 'ádfasdf', '<p>sdfasdf</p>\r\n', 4),
(3, 'Thông báo tuyển dụng', '<p>K&iacute;nh mời bạn đến dự buổi phỏng vấn của c&ocirc;ng ty ch&uacute;ng t&ocirc;i!</p>\r\n\r\n<p>Thời gian:&nbsp;&nbsp;15h ng&agrave;y 31/6/2013&nbsp;</p>\r\n\r\n<p>Địa điểm : Số nh&agrave; 71-Cửu Việt 1- Tr&acirc;u Quỳ - Gia L&acirc;m - H&agrave; Nội</p>\r\n', 4);

-- --------------------------------------------------------

--
-- Table structure for table `tblthongbao_nguoitimviec`
--

CREATE TABLE IF NOT EXISTS `tblthongbao_nguoitimviec` (
  `MaTB_NTV` int(11) NOT NULL AUTO_INCREMENT,
  `MaTB` int(11) NOT NULL,
  `MaNTV` int(11) NOT NULL,
  `ThoiGianGui` datetime NOT NULL,
  `TrangThai` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaTB_NTV`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tblthongbao_nguoitimviec`
--

INSERT INTO `tblthongbao_nguoitimviec` (`MaTB_NTV`, `MaTB`, `MaNTV`, `ThoiGianGui`, `TrangThai`) VALUES
(2, 3, 16, '2013-06-17 01:09:27', 'Đã xem');

-- --------------------------------------------------------

--
-- Table structure for table `tbltinhthanh`
--

CREATE TABLE IF NOT EXISTS `tbltinhthanh` (
  `MaTT` int(11) NOT NULL AUTO_INCREMENT,
  `TenTT` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaTT`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Dumping data for table `tbltinhthanh`
--

INSERT INTO `tbltinhthanh` (`MaTT`, `TenTT`) VALUES
(5, 'Bạc Liêu'),
(4, 'Bà Rịa - Vũng Tàu'),
(3, 'An Giang'),
(2, 'Hồ Chí Minh'),
(1, 'Hà Nội'),
(6, 'Bắc Giang'),
(7, 'Bắc Kạn'),
(8, 'Bắc Ninh'),
(9, 'Bến Tre'),
(10, 'Bình Dương'),
(11, 'Bình Định'),
(12, 'Bình Phước'),
(13, 'Bình Thuận'),
(14, 'Cao Bằng'),
(15, 'Cà Mau'),
(16, 'Cần Thơ'),
(17, 'Hải Phòng'),
(18, 'Đà Nẵng'),
(19, 'Gia Lai'),
(20, 'Hòa Bình'),
(21, 'Hà Giang'),
(22, 'Hà Nam'),
(23, 'Hà Tĩnh'),
(24, 'Hưng Yên'),
(25, 'Hải Dương'),
(26, 'Hậu Giang'),
(27, 'Điện Biên '),
(28, 'Đắk Lắk'),
(29, 'Đắk Nông'),
(30, 'Đồng Nai'),
(31, 'Đồng Tháp'),
(32, 'Khánh Hòa'),
(33, 'Kiên Giang'),
(34, 'Kon Tum'),
(35, 'Lai Châu'),
(36, 'Long An'),
(37, 'Lào Cai'),
(38, 'Lâm Đồng'),
(39, 'Lạng Sơn'),
(40, 'Nam Định'),
(41, 'Nghệ An'),
(42, 'Ninh Bình'),
(43, 'Ninh Thuận'),
(44, 'Phú Thọ'),
(45, 'Phú Yên'),
(46, 'Quảng Bình'),
(47, 'Quảng Nam '),
(48, 'Quảng Ngãi'),
(49, 'Quảng Ninh'),
(50, 'Quảng Trị'),
(51, 'Sóc Trăng'),
(52, 'Sơn La'),
(53, 'Thanh Hóa'),
(54, 'Thái Bình'),
(55, 'Thái Nguyên'),
(56, 'Thừa Thiên - Huế'),
(57, 'Tiền Giang'),
(58, 'Trà Vinh'),
(59, 'Tuyên Quang'),
(60, 'Tây Ninh'),
(61, 'Vĩnh Long'),
(62, 'Vĩnh Phúc'),
(63, 'Yên Bái');

-- --------------------------------------------------------

--
-- Table structure for table `tbltuvanvien`
--

CREATE TABLE IF NOT EXISTS `tbltuvanvien` (
  `MaTVV` int(11) NOT NULL AUTO_INCREMENT,
  `TenTVV` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `yahoo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Dienthoai` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `NoiHoatDong` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `TrangThai` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`MaTVV`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbltuvanvien`
--

INSERT INTO `tbltuvanvien` (`MaTVV`, `TenTVV`, `email`, `yahoo`, `skype`, `Dienthoai`, `NoiHoatDong`, `TrangThai`) VALUES
(6, 'Nguyễn Thành Trung', 'nguyenthanhtrung.dthn@gmail.com', 'faithful_gaby_34', 'mrtrung', '01655052774', 'NTD', 'Active'),
(2, 'Nguyễn Văn Thiện', 'nvthien55a@gmail.com', 'thienpro_9x', 'thienpro_9x', '01664289541', 'NTD', 'active'),
(3, 'Phạm Văn Quang', 'quang@gmail.com', 'quangpham9x', 'quangpham9x', '01655052787', 'NTD', 'active'),
(4, 'Nguyễn Thanh Thủy', 'thanhthuy_1709@yahoo.com', 'thanhthuy_1709', 'thanhthuy_1709', '01699857475', 'NTD', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE IF NOT EXISTS `tbluser` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Pwd` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `TenNQT` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `DienThoai` varchar(11) NOT NULL,
  `Quyen` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`ID`, `Email`, `Pwd`, `TenNQT`, `DienThoai`, `Quyen`) VALUES
(1, 'trung@gmail.com', '123456', 'MrTrung', '098342423', 'admin'),
(2, 'thien@gmail.com', '123456', 'Nguyễn Văn Thiện', '096854274', 'Mod');

-- --------------------------------------------------------

--
-- Table structure for table `tblvieclam`
--

CREATE TABLE IF NOT EXISTS `tblvieclam` (
  `MaVL` int(11) NOT NULL AUTO_INCREMENT,
  `TieuDe` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `SoLuongTuyen` int(11) NOT NULL,
  `LoaiVL` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ViTri` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MoTa` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `MucLuong` int(20) NOT NULL,
  `DVTinhLuong` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `QuyenLoi` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `YCBangCap` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `YCKiNangChuyenMon` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `YCKinhNghiem` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `YCGioiTinh` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `YCDoTuoi` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `YCKhac` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `YCHoSo` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `HanNopHS` date NOT NULL,
  `HinhThucNop` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `NgayDang` date NOT NULL,
  `LuotXem` int(11) NOT NULL,
  `TrangThai` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `MaNTD` int(11) NOT NULL,
  `TenNLH` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EmailNLH` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DiaChiNLH` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SoDT` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NgaySuaDoi` date NOT NULL,
  `SoNgayDang` tinyint(2) NOT NULL,
  `MaTT` int(11) NOT NULL,
  `MaNganh` int(11) NOT NULL,
  PRIMARY KEY (`MaVL`),
  KEY `TieuDe` (`TieuDe`),
  KEY `HinhThucNop` (`HinhThucNop`),
  KEY `HanNopHS` (`HanNopHS`),
  KEY `fk1` (`MaNTD`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `tblvieclam`
--

INSERT INTO `tblvieclam` (`MaVL`, `TieuDe`, `SoLuongTuyen`, `LoaiVL`, `ViTri`, `MoTa`, `MucLuong`, `DVTinhLuong`, `QuyenLoi`, `YCBangCap`, `YCKiNangChuyenMon`, `YCKinhNghiem`, `YCGioiTinh`, `YCDoTuoi`, `YCKhac`, `YCHoSo`, `HanNopHS`, `HinhThucNop`, `NgayDang`, `LuotXem`, `TrangThai`, `MaNTD`, `TenNLH`, `EmailNLH`, `DiaChiNLH`, `SoDT`, `NgaySuaDoi`, `SoNgayDang`, `MaTT`, `MaNganh`) VALUES
(21, 'Kỹ Sư Phát Triển Phần Mềm dotNet', 12, 'Bán thời gian cố định', 'Nhan vien', 'asfdasdf', 0, 'Thỏa thuận', 'asdfasdf', 'Trung học', 'fasfdasdf', 'Không yêu cầu', 'Nữ', '25', 'sdfasdf', 'sdafsadfasdf', '2013-05-31', 'Nộp trực tuyến|', '0000-00-00', 0, 'Đã đăng', 4, 'asds', 'trung@gmail.com', 'fasdfasd', '1215464', '0000-00-00', 0, 1, 8),
(22, 'Nhân Viên Kinh Doanh', 0, '', '', '', 0, '', '', '', '', '', '', '0', '', '', '0000-00-00', '', '0000-00-00', 0, '', 6, NULL, NULL, NULL, NULL, '0000-00-00', 0, 2, 1),
(23, 'Nhân Viên Quản Lý Tiếng Nhật', 0, '', '', '', 0, '', '', '', '', '', '', '0', '', '', '0000-00-00', '', '0000-00-00', 0, '', 5, NULL, NULL, NULL, NULL, '0000-00-00', 0, 12, 1),
(24, 'Trưởng Phòng Kỹ Thuật (Nhiệt Lạnh) ', 0, '', '', '', 0, '', '', '', '', '', '', '0', '', '', '0000-00-00', '', '0000-00-00', 0, '', 7, NULL, NULL, NULL, NULL, '0000-00-00', 0, 1, 12),
(25, 'Chuyên Viên Kinh Doanh BĐS', 0, '', '', '', 0, '', '', '', '', '', '', '0', '', '', '0000-00-00', '', '0000-00-00', 0, '', 9, NULL, NULL, NULL, NULL, '0000-00-00', 0, 9, 1),
(26, 'Chuyên Viên Tư Vấn Tuyển Sinh', 0, '', '', '', 0, '', '', '', '', '', '', '0', '', '', '0000-00-00', '', '0000-00-00', 0, '', 10, NULL, NULL, NULL, NULL, '0000-00-00', 0, 29, 1),
(27, 'Nhân Viên Kỹ Thuật Bia Tươi', 0, '', '', '', 0, '', '', '', '', '', '', '0', '', '', '0000-00-00', '', '0000-00-00', 0, '', 7, NULL, NULL, NULL, NULL, '0000-00-00', 0, 1, 2),
(28, 'Tuyển nhân viên bán hàng tại các siêu thị', 100, 'Bán thời gian cố định', 'Nhân viên', 'Bán hàng tại các siêu thị trên toàn quốc', 5000000, 'VND', 'Có phụ cấp, được đóng bảo hiểm.\r\nCó cơ hội thăng tiến', 'Trung học', '-Làm việc theo nhóm và độc lập\r\n-Kĩ năng giao tiép tốt', 'Không yêu cầu', 'Nữ', '18', 'Ưu tiên những ứng viên có khả năng giao tiếp bằng tiếng Anh', '2 ảnh 3x4\r\nsơ yếu lý lịch\r\nbằng cấp liên quan\r\ngiấy khám sức khỏe', '2013-06-30', 'Nộp trực tuyến|', '2013-06-09', 0, 'Đã đăng', 4, 'Mr Trung', 'trung@gmail.com', 'THB-K53 - ĐH Nông nghiệp Hà Nội', '01655052774', '2013-06-02', 30, 1, 1),
(29, 'Kĩ thuật viên', 5, 'Toàn thời gian tạm thời', 'Nhân viên', '- Nhân viên kĩ thuật', 5000000, 'VND', '- Được hưởng phụ cấp\r\n- Được đóng bảo hiểm y tế', 'PTCS', '- Thành thạo kĩ năng lắp rạp các thiết bị điện tử điện lạnh', 'Không yêu cầu', 'Nam', '18', '- Cần cù chăm chỉ\r\n- Có khả năng chịu áp lực công việc', '- Sơ yếu lý lịch\r\n- Giấy khám sức khỏe\r\n- Các bằng cấp liên quan', '2013-06-30', 'Nộp trực tuyến|', '2013-06-17', 12, 'Đã đăng', 5, 'Mr Trung', 'trung@gmail.com', 'THB-K53 - ĐH Nông nghiệp Hà Nội', '01655052774', '2013-06-02', 30, 1, 13),
(30, 'Kế toán thực tập - Kế toán tổng hợp', 5, 'Toàn thời gian cố định', 'Nhân viên', 'Các bạn sinh viên năm cuối các trường TC-CĐ-ĐH chưa có chỗ thực tập làm đề tài luận án tốt nghiệp, liên hệ công ty Thanh Trí để được nhận việc kế toán thực tập (các bạn vào trang web : thanhtriketoan.net hoặc thanhtriketoan.com để xem điều kiện để được thực tập)   - Các bạn được cung cấp các hóa đơn, chứng từ thực tế của các công ty sản xuất - thương mại - dịch vụ (các Cty này hiện đang nhờ Cty Thanh Trí làm dịch vụ kế toán) để các bạn làm đề tài thực tập. (Ví dụ đề tài : Xác định kết quả KD ; Tiền lương và các khoản trích theo lương; Phân tích báo cáo tài chính; Kế toán vốn bằng tiền; Kế toán thuế GTGT; Biện pháp nâng cao hiệu quả KD …). Sau khi hoàn thành đề tài sẽ được nhận xét, ký tên, đóng dấu để nộp cho nhà trường.  - Thực tập làm báo cáo thuế, báo cáo quý, báo cáo tài chính năm, làm và in các loại sổ sách kế toán bằng chứng từ, hóa đơn thực tế của Cty.  -Thực tập cách thức nộp báo cáo thuế qua mạng (Cty Kế Toán Thanh Trí là đại lý chuyên cung cấp dịch vụ chữ ký số, USB Token các mạng VNPT, FPT, CK-CA... cho các DN)', 0, 'Thỏa thuận', 'Theo quy định của công ty', 'Cao đẳng', 'Không yêu cầu', '1 năm', 'Nam/Nữ', '0', 'Không có', 'Mọi thắc mắc vui lòng liện hệ Công ty Thanh Trí hoặc truy cập web :www. thanhtriketoan.com để được hướng dẫn chi tiết', '2013-06-29', 'Nộp trực tuyến|', '2013-06-06', 21, 'Đã đăng', 10, 'Mr Trung', 'trung@gmail.com', 'Khoa Kế toán - ĐH Nông nghiệp', '01655052774', '2013-06-06', 30, 1, 18);

-- --------------------------------------------------------

--
-- Table structure for table `tblvieclamdaluu`
--

CREATE TABLE IF NOT EXISTS `tblvieclamdaluu` (
  `MaVLDL` int(11) NOT NULL AUTO_INCREMENT,
  `MaVL` int(11) NOT NULL,
  `MaNTV` int(11) NOT NULL,
  PRIMARY KEY (`MaVLDL`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `tblvieclamdaluu`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
