<?php
	$tplSignup= new Xtemplate('view/account/signup.htm');
	global $dbs;
	global $appdb;
	global $baseUrl;
	global $mailvalid;
	$errMes='';
	$do_save=0;
		$url='./img/logo_employer/';
		$allowType=array('jpg','png','jpeg');
		$maxSize=1024000;
	if($_POST){
		$flogo=!empty($_FILES['flogo'])?$_FILES['flogo']:'';
		$email=$_POST['txtEmail'];
		$pwd=$_POST['txtMatkhau'];
		$apwd=$_POST['txtNhapLaiMK'];
		$TenNTD=$_POST['txtTenNTD'];
		$Website=$_POST['txtWebsite'];
		$Quymo=$_POST['txtQuymo'];
		$DiaChi=$_POST['txtDiaChi'];
		$DienThoaiLH=$_POST['txtDienThoaiLH'];
		$SoLuocVeNTD=$_POST['txtSoLuocVeNTD'];
		
		
		$sql="SELECT Email FROM tblNhaTuyenDung WHERE Email='$email'";
		$arrEmail=$dbs->fetchAll($sql);
		
		$sender = 'nguyenthanhtrung.dthn@gmail.com';
		// instantiate the class
		// turn on debugging if you want to view the SMTP transaction
		/*$mailvalid->debug = true;*/
		// do the validation
		$results = $mailvalid->validate(array($email), $sender);
		if(!$email){
			$errMes.='<li>Email không được để trống</li>';
			$do_save=-1;
			}
			elseif(!isEmail($email)){
				$errMes.='<li>Email không hợp lệ</li>';
				$do_save=-1;
			}
				elseif(!$results[$email]){
					$errMes.='<li>Email không tồn tại</li>';
					$do_save=-1;
				}
				elseif($arrEmail!='-1'){
					$errMes.='<li>Email đã được đăng ký</li>';
					$do_save=-1;
				}
				
		if(!$pwd){
			$errMes.='<li>Mật khẩu không được để trống</li>';
			$do_save=-1;	
		}
		if(!$apwd){
			$errMes.='<li>Nhập lại mật khẩu không được để trống</li>';
			$do_save=-1;
			}
			elseif($pwd!=$apwd){
				$errMes.='<li>Trường Mật khẩu và Nhập lại mật khẩu phải giống nhau</li>';
				$do_save=-1;
			}	
		if(!$TenNTD){
			$errMes.='<li>Tên nhà tuyển dụng không được để trống</li>';
			$do_save=-1;	
		}
		if(!$Quymo){
			$errMes.='<li>Hãy nhập vào số nhân viên của công ty</li>';
			$do_save=-1;	
		}else if(!preg_match("/^[0-9]+$/", $Quymo)){
				$errMes.='<li>Quy mô công ty phải là số nguyên</li>';
				$do_save=-1;		
				
		}
		if(!$DiaChi){
			$errMes.='<li>Địa chỉ không được để trống</li>';
			$do_save=-1;	
		}
		if(!$DienThoaiLH){
			$errMes.='<li>Điện thoại liên hệ không được để trống</li>';
			$do_save=-1;
			}
			else if(!preg_match("/^[0-9]+$/", $DienThoaiLH)){
				$errMes.='<li>Điện thoại liên hệ không hợp lệ</li>';
				$do_save=-1;		
				
		}
		if(!$SoLuocVeNTD){
			$errMes.='<li>Hãy nhập thông tin về công ty của bạn</li>';
			$do_save=-1;	
		}
		
		if($errMes!=''){
			$tplSignup->assign('errMes',$errMes);
		}
		
		if($do_save!=-1){
			if($flogo['name']!=''){
				$file_logoNTD=uploadFile($flogo, $url, $allowType, $maxSize);
				if($file_logoNTD=='110i'){
					alert('Kich cỡ file quá lớn');
				}
				if($file_logoNTD=='1200'){
					alert('Định dang file không hợp lệ');
				}
			}
			
			$arrData=array('Email'=>$email
							,'Pwd'=>$pwd
							,'TenNTD'=>$TenNTD
							,'DiaChi'=>$DiaChi
							,'DienThoaiLH'=>$DienThoaiLH
							,'Website'=>$Website
							,'LogoLink'=>$file_logoNTD
							,'SoLuocVeNTD'=>$SoLuocVeNTD
							,'QuyMo'=>$Quymo);
			
			//Save data to tbladvetising
			if($dbs->insert('tblNhatuyendung',$arrData)){
				alert('Chúc mừng! Bạn đã đăng ký thành công! Hãy đăng nhập để truy cập hệ thống.');
				redir('?m=account&a=login');
			}
		}
		//Gan gia tri cac bien sau khi post
		$tplSignup->assign('Email',$email);
		$tplSignup->assign('Pwd',$pwd);
		$tplSignup->assign('TenNTD',$TenNTD);
		$tplSignup->assign('DiaChi',$DiaChi);
		$tplSignup->assign('DienThoaiLH',$DienThoaiLH);
		$tplSignup->assign('Website',$Website);
		$tplSignup->assign('SoLuocVeNTD',$SoLuocVeNTD);
		$tplSignup->assign('Logo',$flogo['name']);
		$tplSignup->assign('QuyMo',$Quymo);
	}
	$tplSignup->parse('SIGNUP');
	$left_content= $tplSignup->text('SIGNUP');
?>
